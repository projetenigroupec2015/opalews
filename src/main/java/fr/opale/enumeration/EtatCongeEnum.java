package fr.opale.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Brice on 07/06/2016.
 */
public enum EtatCongeEnum {
    EN_ATTENTE(1, "attente"),
    VALIDE (2, "valide"),
    REFUSE(3, "refuse");


    private int numero;
    private String description;

    public int getNumber() {
        return numero;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return getDescription();
    }

    EtatCongeEnum(int numero, String description) {
        this.numero = numero;
        this.description = description;
    }


    private static final Map<Integer, EtatCongeEnum> mapNumero = new HashMap<>();
    private static final Map<String, EtatCongeEnum> mapDescription = new HashMap<>();
    static {
        for (EtatCongeEnum etat : values()) {
            mapNumero.put(etat.getNumber(), etat);
            mapDescription.put(etat.getDescription(), etat);
        }
    }

    public static EtatCongeEnum getEnum(int numero) {
        return mapNumero.get(numero);
    }
    public static EtatCongeEnum getEnum(String description) {
        return mapDescription.get(description);
    }
}
