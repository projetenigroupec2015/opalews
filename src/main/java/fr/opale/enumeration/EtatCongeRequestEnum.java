package fr.opale.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Brice BRUNEAU
 * Date : 24/07/2016
 * Package : fr.opale.enumeration
 */
public enum EtatCongeRequestEnum {
    ALL ("all"),
    ACTIF ("actif");

    private String description;

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return getDescription();
    }

    EtatCongeRequestEnum(String description) {
        this.description = description;
    }

    private static final Map<String, EtatCongeRequestEnum> mapDescription = new HashMap<>();
    static {
        for (EtatCongeRequestEnum etat : values()) {
            mapDescription.put(etat.getDescription(), etat);
        }
    }

    public static EtatCongeRequestEnum getEtatCongeRequeteEnum(String description) {
        return mapDescription.get(description);
    }
}
