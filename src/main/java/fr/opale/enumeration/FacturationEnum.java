package fr.opale.enumeration;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by JEREMY on 16/10/2016.
 */
public enum FacturationEnum {
    DEMANDE (1,"Demandé"),
    VALIDE (2,"Validé"),
    FACTURE (3,"Facturé");

    private int numero;
    private String description;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    FacturationEnum(int numero, String description) {
        this.numero = numero;
        this.description = description;
    }

    private static final Map<Integer, FacturationEnum> mapNumero = new HashMap<>();
    private static final Map<String, FacturationEnum> mapDescription = new HashMap<>();
    static {
        for (FacturationEnum etat : values()) {
            mapNumero.put(etat.getNumero(), etat);
            mapDescription.put(etat.getDescription(), etat);
        }
    }
    public static FacturationEnum getEtatCongeEnum(int numero) {
        return mapNumero.get(numero);
    }
    public static FacturationEnum getEtatCongeEnum(String description) {
        return mapDescription.get(description);
    }
}
