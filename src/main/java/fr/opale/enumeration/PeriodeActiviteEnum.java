package fr.opale.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Brice BRUNEAU
 * Date : 31/07/2016
 * Package : fr.opale.enumeration
 */
public enum PeriodeActiviteEnum {
    MATIN(1, "matin"),
    APRES_MIDI (2, "après-midi");

    private int numero;
    private String description;

    public int getNumber() {
        return numero;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return getDescription();
    }

    PeriodeActiviteEnum(int numero, String description) {
        this.numero = numero;
        this.description = description;
    }

    private static final Map<Integer, PeriodeActiviteEnum> mapNumero = new HashMap<>();
    private static final Map<String, PeriodeActiviteEnum> mapDescription = new HashMap<>();
    static {
        for (PeriodeActiviteEnum periode : values()) {
            mapNumero.put(periode.getNumber(), periode);
            mapDescription.put(periode.getDescription(), periode);
        }
    }

    public static PeriodeActiviteEnum getEnum(int numero) {
        return mapNumero.get(numero);
    }

    public static PeriodeActiviteEnum getEnum(String description) {
        return mapDescription.get(description);
    }
}
