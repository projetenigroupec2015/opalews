package fr.opale.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by JEREMY on 18/10/2016.
 */

public enum NotificationEnum {
    ALERTE(1,"Alertes"),
    CONGE(2,"Congés"),
    FICHE_TEMPS(3,"Fiche de temps");

    private int numero;
    private String description;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    NotificationEnum(int numero, String description) {
        this.numero = numero;
        this.description = description;
    }

    private static final Map<Integer, NotificationEnum> mapNumero = new HashMap<>();
    private static final Map<String, NotificationEnum> mapDescription = new HashMap<>();
    static {
        for (NotificationEnum etat : values()) {
            mapNumero.put(etat.getNumero(), etat);
            mapDescription.put(etat.getDescription(), etat);
        }
    }
    public static NotificationEnum getNotificationEnum(int numero) {
        return mapNumero.get(numero);
    }
    public static NotificationEnum getNotificationEnum(String description) {
        return mapDescription.get(description);
    }
}
