package fr.opale.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Thomas Croguennec
 * Date : 17/06/16
 * Package : fr.opale.spring
 */
public class CsrfFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsrfFilter.class);

    public static final String XSRF_TOKEN_COOKIE_NAME = "XSFR-TOKEN";

    public static final String XSRF_TOKEN_HEADER_NAME = "X-XSRF-TOKEN";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        LOGGER.debug("Inside CsrfFilter....");
        CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());

        if (csrf != null) {

            String token = csrf.getToken();
            if (token != null) {
                Cookie cookie = new Cookie(XSRF_TOKEN_COOKIE_NAME, token);
                cookie.setPath("/");
                response.addCookie(cookie);

                response.addHeader(XSRF_TOKEN_HEADER_NAME, token);
                LOGGER.debug("Sending CSRF token to client: " + token);
            }

        }
        filterChain.doFilter(request, response);
    }
}
