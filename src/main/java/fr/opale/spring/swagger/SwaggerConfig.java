package fr.opale.spring.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : fr.opale.spring.swagger
 * <br/>
 * La documentation est accessible à l'url : localhost:port/swagger-ui.html
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("fr.opale.web.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "API OPALE",
                "API de l'application OPALE réalisée par le groupe C",
                "v1",
                "Néant",
                new Contact("Groupe C", "", "projetc@campus-eni.fr"),
                "Licence GNU/GPL 3.0",
                "http://www.gnu.org/licenses/gpl-3.0.fr.html");
    }
}
