package fr.opale.spring;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import java.io.File;

/**
  * Created by Brice BRUNEAU
  * Date : 20/10/2016
  * Package : fr.opale.spring
  */
public class SchemaGenerator {

    /**
      * @param args
      */
    public static void main(String[] args) throws Exception
    {
        SchemaGenerator gen = new SchemaGenerator("fr.opale.persistence.model");
        gen.generate();
    }


    private static Logger log = LoggerFactory.getLogger(SchemaGenerator.class);
    private MetadataSources metadata;

    public SchemaGenerator(String packageName) throws Exception
    {
        metadata = new MetadataSources(
                        new StandardServiceRegistryBuilder()
                                .applySetting("hibernate.dialect", Dialect.SQLServer)
                                .build());

        final Reflections reflections = new Reflections(packageName);
        for (Class<?> cl : reflections.getTypesAnnotatedWith(MappedSuperclass.class)) {
            metadata.addAnnotatedClass(cl);
            log.info("Mapped = " + cl.getName());
        }
        for (Class<?> cl : reflections.getTypesAnnotatedWith(Entity.class)) {
            metadata.addAnnotatedClass(cl);
            log.info("Mapped = " + cl.getName());
        }
    }

    /**
      * Method that actually creates the file.
      */
    private void generate()
    {
        File f = new File(System.getProperty("user.dir"));
        String directory = f.getAbsoluteFile()+ "/src/main/resources/sql/generated/";

        SchemaExport export = new SchemaExport(
                (MetadataImplementor) metadata.buildMetadata()
        );

        export.setDelimiter(";");
        export.setFormat(true);

        export.setOutputFile(directory + "opale_create.sql");
        export.execute(true, false, false, true);

        export.setOutputFile(directory + "opale_drop.sql");
        export.execute(true, false, true, false);
    }

    /**
      * Holds the classnames of hibernate dialects for easy reference.
      */
    private enum Dialect
    {
        ORACLE("org.hibernate.dialect.Oracle10gDialect"),
        MYSQL("org.hibernate.dialect.MySQLDialect"),
        HSQL("org.hibernate.dialect.HSQLDialect"),
        SQLServer("org.hibernate.dialect.SQLServerDialect");

        private String dialectClass;
        private Dialect(String dialectClass)
        {
            this.dialectClass = dialectClass;
        }
        public String getDialectClass()
        {
            return dialectClass;
        }
    }
}
