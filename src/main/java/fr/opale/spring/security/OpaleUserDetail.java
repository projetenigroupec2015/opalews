package fr.opale.spring.security;

import fr.opale.persistence.model.Formateur;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

/**
 * Created by Thomas Croguennec
 * Date : 01/09/16
 * Package : fr.opale.spring.security
 */
public class OpaleUserDetail implements UserDetails {


    private static final long serialVersionUID = 1269663192591262347L;
    private Formateur formateur;

    Set<GrantedAuthority> authorities = null;

    public OpaleUserDetail(Formateur formateur) {
        this.formateur = formateur;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return formateur.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
