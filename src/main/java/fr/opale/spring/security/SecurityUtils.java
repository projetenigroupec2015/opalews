package fr.opale.spring.security;

import fr.opale.persistence.model.Formateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.naming.directory.DirContext;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

/**
 * Created by Thomas Croguennec
 * Date : 31/08/16
 * Package : fr.opale.spring.security
 */
@Service
public class SecurityUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtils.class);

    private static final ResourceBundle LDAP = ResourceBundle.getBundle("ldap");

    //Adresse du LDAP, preciser le port si le LDAP n'est pas defini avec celui par defaut
    final private String ldapAddr = LDAP.getString("url");
    //DC du LDAP + le premier OU
    final private String domainComponents = LDAP.getString("dc");
    //Domaine du LDAP
    final private String domainName = LDAP.getString("dn");

    public SecurityUtils() {
    }

    public String getCurrentLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        OpaleUserDetail user = null;
        String userName = null;
        if (authentication != null) {
            userName = (String) authentication.getPrincipal();
        } else if (authentication.getPrincipal() instanceof String) {
            userName = (String) authentication.getPrincipal();
        }
        return userName;
    }

    public boolean isLDAPAuthorize(String username, String password) {
        boolean authed = false;
        DirContext ctx = null;
        LdapContextSource contextSource = null;

        if (username != null) {
            LOGGER.info("Authentification du user : " + username);
        }

        try {
            contextSource = new LdapContextSource();
            contextSource.setUrl(ldapAddr);
            contextSource.setBase(domainComponents);
            contextSource.setUserDn(username + "@" + domainName);
            contextSource.setPassword(password);
            contextSource.afterPropertiesSet();

//            ctx = contextSource.getContext(username + "@" + domainName, password);

            LdapTemplate ldapTemplate = new LdapTemplate(contextSource);
            ldapTemplate.afterPropertiesSet();

            ldapTemplate.authenticate(query().where("sAMAccountName").is(username), password);
            authed = true;
        } catch (Exception e) { // Si l'authentification fail
            LOGGER.error("Echec d'authentification : {}", e.getMessage());
            authed = false;
        }

        if (authed) {
            LOGGER.info("Authentification réussie !");
        }

        return authed;
    }

    public List<GrantedAuthority> addAuthorities(Formateur formateur) {
        LOGGER.debug("addAuthorities");
        List<GrantedAuthority> grantedAuthorities = null;
        grantedAuthorities = new ArrayList<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(formateur.getHabilitation().getLibelle()));
        return grantedAuthorities;
    }
}
