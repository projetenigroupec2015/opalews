package fr.opale.spring.security;

import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.FormateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Thomas Croguennec
 * Date : 30/08/16
 * Package : fr.opale.spring.security
 */
@Component("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsService.class);

    @Autowired
    private FormateurService formateurService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Formateur user = formateurService.findByUsername(login);
        LOGGER.debug("UserDetail");
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(user.getHabilitation().getLibelle());
        grantedAuthorities.add(grantedAuthority);

        return new OpaleUserDetail(user);
    }
}
