package fr.opale.spring.security;

import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.FormateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 31/08/16
 * Package : fr.opale.spring.security
 */
@Component
public class OpaleAuthoritiesPopulator implements LdapAuthoritiesPopulator {

    @Autowired
    private FormateurService formateurService;

    @Override
    public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations dirContextOperations, String username) {
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        final Formateur formateur = formateurService.findByUsername(username);
        if(formateur != null){
            authorities.add(new SimpleGrantedAuthority(formateur.getHabilitation().getLibelle()));
        }
        return authorities;
    }
}
