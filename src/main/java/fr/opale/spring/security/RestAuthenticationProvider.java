package fr.opale.spring.security;

import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.FormateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 31/08/16
 * Package : fr.opale.spring.security
 */
@Component
public class RestAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestAuthenticationProvider.class);

    @Autowired
    private FormateurService formateurService;

    @Autowired
    private SecurityUtils secu;

    @Transactional
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.debug("AUTHENTICATION");
        List<GrantedAuthority> grantedAuthorities = null;
        Formateur formateur = null;
        boolean authenticated = false;
        String username = (String) authentication.getPrincipal();
        String credential = authentication.getCredentials().toString();
        if (username != null) {
            LOGGER.debug("Recherche du formateur");
            formateur = formateurService.findByUsername(username);
        }
        if (formateur == null) {
            throw new BadCredentialsException("Identifiant inconnu.");
        } else {
            grantedAuthorities = secu.addAuthorities(formateur);
            authenticated = secu.isLDAPAuthorize(username, credential);
            if(authenticated){
                return new UsernamePasswordAuthenticationToken(username, null, grantedAuthorities);
            } else {
                throw new BadCredentialsException("Mot de passe incorrect.");
            }
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
