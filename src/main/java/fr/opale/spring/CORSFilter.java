package fr.opale.spring;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Thomas Croguennec
 * Date : 17/06/16
 * Package : fr.opale.spring
 */
@Configuration
@Component
public class CORSFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CORSFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        LOGGER.debug("Inside CORSFilter...");

        Cors cors = new Cors();

        String origin = httpServletRequest.getHeader("Origin");

        httpServletResponse.setHeader("Access-Control-Allow-Origin", origin);
        httpServletResponse.setHeader("Access-Control-Allow-Methods", StringUtils.join(cors.getAllowedMethods(), ","));
        httpServletResponse.setHeader("Access-Control-Allow-Headers", StringUtils.join(cors.getAllowedHeaders(), ","));
        httpServletResponse.setHeader("Access-Control-Expose-Headers", StringUtils.join(cors.getExposedHeaders(), ","));
        httpServletResponse.setHeader("Access-Control-Max-Age", Long.toString(cors.getMaxAge()));
        httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");

        if (!httpServletRequest.getMethod().equals("OPTIONS"))
            filterChain.doFilter(httpServletRequest, httpServletResponse);

    }

    public static class Cors {

        /**
         * Comma separated whitelisted URLs for CORS.
         * Should contain the applicationURL at the minimum.
         * Not providing this property would disable CORS configuration.
         */
        private String[] allowedOrigins;

        /**
         * Methods to be allowed, e.g. GET,POST,...
         */
        private String[] allowedMethods = {"GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "OPTIONS", "PATCH"};

        /**
         * Request headers to be allowed, e.g. content-type,accept,origin,x-requested-with,x-xsrf-token,...
         */
        private String[] allowedHeaders = {
                "Accept",
                "Accept-Encoding",
                "Accept-Language",
                "Authorization",
                "Cache-Control",
                "Connection",
                "Content-Length",
                "Content-Type",
                "Cookie",
                "Host",
                "Origin",
                "Pragma",
                "Referer",
                "User-Agent",
                "X-requested-with",
                CsrfFilter.XSRF_TOKEN_HEADER_NAME};

        /**
         * Response headers that you want to expose to the client JavaScript programmer, e.g. "X-XSRF-TOKEN".
         * I don't think we need to mention here the headers that we don't want to access through JavaScript.
         * Still, by default, we have provided most of the common headers.
         *
         * <br>
         * See <a href="http://stackoverflow.com/questions/25673089/why-is-access-control-expose-headers-needed#answer-25673446">
         * here</a> to know why this could be needed.
         */
        private String[] exposedHeaders = {
                "Cache-Control",
                "Connection",
                "Content-Type",
                "Date",
                "Expires",
                "Pragma",
                "Server",
                "Set-Cookie",
                "Transfer-Encoding",
                "X-Content-Type-Options",
                "X-XSS-Protection",
                "X-Frame-Options",
                "X-Application-Context",
                CsrfFilter.XSRF_TOKEN_HEADER_NAME};

        /**
         * CORS <code>maxAge</code> long property
         */
        private long maxAge = 3600L;

        public String[] getAllowedOrigins() {
            return allowedOrigins;
        }

        public void setAllowedOrigins(String[] allowedOrigins) {
            this.allowedOrigins = allowedOrigins;
        }

        public String[] getAllowedMethods() {
            return allowedMethods;
        }

        public void setAllowedMethods(String[] allowedMethods) {
            this.allowedMethods = allowedMethods;
        }

        public String[] getAllowedHeaders() {
            return allowedHeaders;
        }

        public void setAllowedHeaders(String[] allowedHeaders) {
            this.allowedHeaders = allowedHeaders;
        }

        public String[] getExposedHeaders() {
            return exposedHeaders;
        }

        public void setExposedHeaders(String[] exposedHeaders) {
            this.exposedHeaders = exposedHeaders;
        }

        public long getMaxAge() {
            return maxAge;
        }

        public void setMaxAge(long maxAge) {
            this.maxAge = maxAge;
        }

    }
}
