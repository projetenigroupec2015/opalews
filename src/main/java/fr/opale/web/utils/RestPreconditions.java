package fr.opale.web.utils;

import fr.opale.web.exception.ResourceNotFoundException;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : ${PACKAGE_NAME}
 *
 * Simple static methods to be called at the start of the methods to verify
 * correct arguments and state. If the Precondition fails, an {@link org.springframework.http.HttpStatus} code is thrown
 */
public class RestPreconditions {

    public static <T> T checkFound(final T resource) {
        if(resource == null) {
            throw new ResourceNotFoundException();
        }
        return resource;
    }

    public static void checkFound(final boolean expression) {
        if(!expression) {
            throw new ResourceNotFoundException();
        }
    }

}
