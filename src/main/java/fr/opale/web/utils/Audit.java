package fr.opale.web.utils;

import fr.opale.persistence.model.PisteAudit;
import fr.opale.persistence.service.PisteAuditService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * OpaleWS - fr.opale.web.utils
 * <p>
 * Created by Thomas Croguennec on 22/10/16.
 * On 22/10/16
 */
@Slf4j
@Component
public class Audit {

    @Autowired
    private PisteAuditService pisteAuditService;

    public void record(String action){
        Authentication user = SecurityContextHolder.getContext().getAuthentication();

        PisteAudit pisteAudit = new PisteAudit();
        pisteAudit.setUser(user.getName());
        pisteAudit.setDate(LocalDateTime.now());
        pisteAudit.setAction(action);

        pisteAuditService.create(pisteAudit);
    }

}
