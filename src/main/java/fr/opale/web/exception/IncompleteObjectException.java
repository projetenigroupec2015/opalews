package fr.opale.web.exception;

/**
 * Created by Brice BRUNEAU
 * Date : 23/10/2016
 * Package : fr.opale.web.exception.global
 */
public class IncompleteObjectException extends RuntimeException {

    private static final String MESSAGE = "L'objet founi possède des attributs non-renseignés.";

    public IncompleteObjectException() {
        super(MESSAGE);
    }

    public IncompleteObjectException(String message) {
        super(message);
    }

    public IncompleteObjectException(Throwable cause) {
        super(MESSAGE, cause);
    }

    public IncompleteObjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncompleteObjectException(Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(MESSAGE, cause, enableSuppression, writableStackTrace);
    }

    public IncompleteObjectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
