package fr.opale.web.exception;

/**
 * Created by Brice BRUNEAU
 * Date : 31/07/2016
 * Package : fr.opale.web.exception
 */
public class IncompatibleObjectAndIdException extends RuntimeException {

    private static final String MESSAGE = "L'Id founi ne correspond pas à celui de l'objet associé.";

    public IncompatibleObjectAndIdException() {
        super(MESSAGE);
    }

    public IncompatibleObjectAndIdException(String message) {
        super(message);
    }

    public IncompatibleObjectAndIdException(Throwable cause) {
        super(MESSAGE, cause);
    }

    public IncompatibleObjectAndIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncompatibleObjectAndIdException(Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(MESSAGE, cause, enableSuppression, writableStackTrace);
    }

    public IncompatibleObjectAndIdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
