package fr.opale.web.exception.conge;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 * Created by Brice BRUNEAU
 * Date : 05/07/2016
 * Package : fr.opale.web.exception.conge
 */
public class CongeIncompatibleException extends RuntimeException {

    private static final String MESSAGE = "Le congé est incompatible avec les congés déjà existant.";

    public CongeIncompatibleException() {
        super(MESSAGE);
    }

    public CongeIncompatibleException(String message) {
        super(message);
    }

    public CongeIncompatibleException(Throwable cause) {
        super(MESSAGE, cause);
    }

    public CongeIncompatibleException(String message, Throwable cause) {
        super(message, cause);
    }

    public CongeIncompatibleException(Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(MESSAGE, cause, enableSuppression, writableStackTrace);
    }

    public CongeIncompatibleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
