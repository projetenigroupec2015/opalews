package fr.opale.web.exception;

/**
 * Created by Brice BRUNEAU
 * Date : 24/07/2016
 * Package : fr.opale.web.exception
 */
public class IncompatibleDateException extends RuntimeException {

    private static final String MESSAGE = "La date de début ne peut être postérieur à la date de fin.";

    public IncompatibleDateException() {
        super(MESSAGE);
    }

    public IncompatibleDateException(String message) {
        super(message);
    }

    public IncompatibleDateException(Throwable cause) {
        super(MESSAGE, cause);
    }

    public IncompatibleDateException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncompatibleDateException(Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(MESSAGE, cause, enableSuppression, writableStackTrace);
    }

    public IncompatibleDateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
