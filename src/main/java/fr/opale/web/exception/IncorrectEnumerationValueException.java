package fr.opale.web.exception;

/**
 * Created by Brice BRUNEAU
 * Date : 24/07/2016
 * Package : fr.opale.web.exception
 */
public class IncorrectEnumerationValueException extends RuntimeException {
    private static final String MESSAGE = "Aucune valeur de l'énumeration ne correspond à la valeur fournie.";

    public IncorrectEnumerationValueException() {
        super(MESSAGE);
    }

    public IncorrectEnumerationValueException(String message) {
        super(message);
    }

    public IncorrectEnumerationValueException(Throwable cause) {
        super(MESSAGE, cause);
    }

    public IncorrectEnumerationValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectEnumerationValueException(Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(MESSAGE, cause, enableSuppression, writableStackTrace);
    }

    public IncorrectEnumerationValueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
