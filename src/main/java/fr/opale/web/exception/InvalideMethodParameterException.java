package fr.opale.web.exception;

/**
 * Created by Brice BRUNEAU
 * Date : 31/07/2016
 * Package : fr.opale.web.exception
 */
public class InvalideMethodParameterException extends RuntimeException {

    private static final String MESSAGE = "La valeur du paramètre fourni est invalide.";

    public InvalideMethodParameterException() {
        super(MESSAGE);
    }

    public InvalideMethodParameterException(String message) {
        super(message);
    }

    public InvalideMethodParameterException(Throwable cause) {
        super(MESSAGE, cause);
    }

    public InvalideMethodParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalideMethodParameterException(Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(MESSAGE, cause, enableSuppression, writableStackTrace);
    }

    public InvalideMethodParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
