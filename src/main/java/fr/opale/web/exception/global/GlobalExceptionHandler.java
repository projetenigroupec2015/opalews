package fr.opale.web.exception.global;

import com.microsoft.sqlserver.jdbc.SQLServerException;
import fr.opale.web.exception.*;
import fr.opale.web.exception.conge.CongeIncompatibleException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

/**
 * Created by Thomas Croguennec
 * Date : 15/10/16
 * Package : fr.opale.spring.security
 */
@Slf4j
@RestControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{

    // API

    // 400

    @ExceptionHandler({ ConstraintViolationException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleBadRequest(final ConstraintViolationException ex, final WebRequest request) {
        log.error("Erreur : {} ", ex.getMessage());
        return new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage());
    }

    @ExceptionHandler({ IncompatibleDateException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public ApiError handleBadRequest(final IncompatibleDateException ex, final WebRequest request) {
        log.error("Warning : {} ", ex.getMessage());
        return new ApiError(HttpStatus.CONFLICT.value(), ex.getMessage());
    }

    @ExceptionHandler({ InvalideMethodParameterException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleBadRequest(final InvalideMethodParameterException ex, final WebRequest request) {
        log.error("Warning : {} ", ex.getMessage());
        return new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler({ IncompatibleObjectAndIdException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public ApiError handleBadRequest(final IncompatibleObjectAndIdException ex, final WebRequest request) {
        log.error("Warning : {} ", ex.getMessage());
        return new ApiError(HttpStatus.CONFLICT.value(), ex.getMessage());
    }

    @ExceptionHandler({ CongeIncompatibleException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public ApiError handleBadRequest(final CongeIncompatibleException ex, final WebRequest request) {
        log.error("Warning : {} ", ex.getMessage());
        return new ApiError(HttpStatus.CONFLICT.value(), ex.getMessage());
    }

    @ExceptionHandler({ IncompleteObjectException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleBadRequest(final IncompleteObjectException ex, final WebRequest request) {
        log.error("Warning : {} ", ex.getMessage());
        return new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler({ IncorrectEnumerationValueException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public ApiError handleBadRequest(final IncorrectEnumerationValueException ex, final WebRequest request) {
        log.error("Warning : {} ", ex.getMessage());
        return new ApiError(HttpStatus.CONFLICT.value(), ex.getMessage());
    }

    @ExceptionHandler({ DataIntegrityViolationException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleBadRequest(final DataIntegrityViolationException ex, final WebRequest request) {
        log.error("Erreur : {} ", ex.getMessage());
        return new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final String bodyOfResponse = "Message illisible ! Exception : " + ex.getLocalizedMessage();
        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final String bodyOfResponse = "La méthode de prend en charge ces paramètres. Exception : " + ex.getLocalizedMessage() ;
        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    // 401
    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ApiError badCredentialsExceptionHandler(BadCredentialsException e) {
        log.error("Erreur : {} ", e.getMessage());
        return new ApiError(HttpStatus.UNAUTHORIZED.value(), e.getLocalizedMessage());
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ApiError authenticationExceptionHandler(AuthenticationException e) {
        log.error("Erreur : {} ", e.getMessage());
        return new ApiError(HttpStatus.UNAUTHORIZED.value(), e.getLocalizedMessage());
    }

    // 403
    @ExceptionHandler({ AccessDeniedException.class })
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ApiError handleAccessDenied(final AccessDeniedException ex, final WebRequest request) {
        log.error("Erreur : {} ", ex.getMessage());
        return new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage());
    }
    // 404

    @ExceptionHandler(value = { EntityNotFoundException.class, ClassNotFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    protected ResponseEntity<Object> handleNotFound(final RuntimeException ex, final WebRequest request) {
        final String bodyOfResponse = "This should be application specific";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    // 409
    @ExceptionHandler({ InvalidDataAccessApiUsageException.class, DataAccessException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    protected ApiError handleConflict(final RuntimeException ex, final WebRequest request) {
        log.error("Erreur : {} ", ex.getMessage());
        return new ApiError(HttpStatus.CONFLICT.value(), ex.getLocalizedMessage());
    }

    // 500
    @ExceptionHandler({ NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class, TransactionException.class, SQLServerException.class })
    public ApiError handleInternal(final RuntimeException exception, final WebRequest request) {
        log.error("Erreur : {} ", exception.getLocalizedMessage());
        return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getLocalizedMessage());
    }

    // Other Exception
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ApiError handleOtherException(Exception exception, WebRequest request){
        log.error("Erreur : {}", exception.getLocalizedMessage());
        return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getLocalizedMessage());

    }
}
