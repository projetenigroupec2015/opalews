package fr.opale.web.exception.global;

import lombok.Data;
import lombok.NonNull;

/**
 * Created by Thomas Croguennec
 * Date : 18/10/16
 * Package : fr.opale.spring
 */
@Data
public class ApiError  {

    @NonNull private Integer status;
    @NonNull private String message;

}
