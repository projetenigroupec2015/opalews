package fr.opale.web.commons;

/**
 * Created by Thomas Croguennec
 * Date : 26/06/16
 * Package : fr.opale.web.commons
 *
 * WS urls
 */
public class ConstantsUrls {

    /**
     * ETL : LOAD DATA FROM ENI DB TO OPALE DB
     */
    public static final String ENDPOINT_ETL = "/etl-load";
    /**
     * CONFIGURATION
     */
    public static final String ENDPOINT_CONFIGURATION = "/configuration";
    public static final String ENDPOINT_ALERTE = "/alerte";
    public static final String ENDPOINT_ALERTES = "/alertes";
    public static final String ENDPOINT_EQUIPEMENT = "/equipement";
    public static final String ENDPOINT_EQUIPEMENTS = "/equipements";
    public static final String ENDPOINT_FORMATEURS = "/formateurs";
    public static final String ENDPOINT_FORMATIONS = "/formations";
    public static final String ENDPOINT_PARAM = "/param";
    public static final String ENDPOINT_PARAMS = "/params";
    public static final String ENDPOINT_LIEU = "/lieu";
    public static final String ENDPOINT_LIEUX = "/lieux";
    public static final String ENDPOINT_SALLE = "/salle";
    public static final String ENDPOINT_SALLES = "/salles";
    public static final String ENDPOINT_MODULES = "/modules";
    public static final String ENDPOINT_SECURITY = "/security";
    public static final String ENDPOINT_PLANNING = "/planning";

    /**
     * CONGE
     */
    public static final String ENDPOINT_CONGES = "/conges";
    public static final String ENDPOINT_FICHETEMPS = "/fichetemps";

}
