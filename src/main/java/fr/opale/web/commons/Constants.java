package fr.opale.web.commons;

/**
 * Created by Thomas Croguennec
 * Date : 26/06/16
 * Package : fr.opale.web.commons
 *
 * Globals constants
 *
 */

public class Constants {

    public static final String API_VERSION = "v1";

}
