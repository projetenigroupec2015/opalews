package fr.opale.web.controller;

import fr.opale.persistence.etl.ETLProcess;
import fr.opale.web.commons.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Thomas Croguennec
 * Date : 30/07/16
 * Package : fr.opale.web.controller
 */
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class ETLController {

    @RequestMapping(value = "/etl-load", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void runETL(){
        ETLProcess loadETL = new ETLProcess();
        loadETL.run();
    }
}
