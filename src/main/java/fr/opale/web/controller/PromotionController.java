package fr.opale.web.controller;

import fr.opale.persistence.model.Promotion;
import fr.opale.persistence.service.PromotionService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 15/09/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class PromotionController {

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/promotions", method = RequestMethod.GET)
    @ResponseBody
    public List<Promotion> getAllPromotions() {
        return promotionService.findAll();
    }

    @RequestMapping(value = "/promotions/{dateStart}/{dateEnd}", method = RequestMethod.GET)
    @ResponseBody
    public List<Promotion> getAllPromotions(@PathVariable(value = "dateStart") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
                                            @PathVariable(value = "dateEnd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd) {
        return promotionService.findAllPromotions(dateStart, dateEnd);
    }

    @RequestMapping(value = "/promotion", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void updatePromotion(@RequestBody Promotion promotion) {
        audit.record("Mise à jour de la promotion " + ReflectionToStringBuilder.reflectionToString(promotion));
        promotionService.update(promotion);
    }
}
