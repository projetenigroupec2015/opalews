package fr.opale.web.controller;

import fr.opale.persistence.model.Equipement;
import fr.opale.persistence.service.EquipementService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 15/09/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class EquipementController {

    @Autowired
    private EquipementService equipementService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/equipement", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateEquipements(@RequestBody final Equipement equipement) {
        equipementService.update(equipement);
    }

    @RequestMapping(value = "/equipement", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void createEquipement(@RequestBody final Equipement equipement) {
        equipementService.create(equipement);
    }

    @RequestMapping(value = "/equipements/{idEquipement}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteEquipements(@PathVariable("idEquipement") int idEquipement) {
        equipementService.deleteById(idEquipement);
    }

    @RequestMapping(value = "/equipements", method = RequestMethod.GET)
    @ResponseBody
    public List<Equipement> getAllEquipements(){
        log.debug("GET ALL EQUIPEMENTS");
        return equipementService.findAll();
    }
}
