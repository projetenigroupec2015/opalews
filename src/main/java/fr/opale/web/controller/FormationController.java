package fr.opale.web.controller;

import com.google.common.base.Preconditions;
import fr.opale.persistence.model.Formation;
import fr.opale.persistence.service.FormationService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 15/09/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class FormationController {

    @Autowired
    private FormationService formationService;

    @RequestMapping(value = "/formations", method = RequestMethod.GET)
    @ResponseBody
    public List<Formation> getAllFormation(){
        return formationService.findAll();
    }

    @RequestMapping(value = "/formation", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createFormation(@RequestBody final Formation formation){
        Preconditions.checkNotNull(formation);
        RestPreconditions.checkFound(formationService.isExisting(formation.getCode()));

    }

}
