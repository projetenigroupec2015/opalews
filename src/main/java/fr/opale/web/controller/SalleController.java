package fr.opale.web.controller;

import fr.opale.persistence.model.Lieu;
import fr.opale.persistence.model.Salle;
import fr.opale.persistence.service.LieuService;
import fr.opale.persistence.service.SalleService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 15/09/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class SalleController {

    @Autowired
    private SalleService salleService;

    @Autowired
    private LieuService lieuService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/salles", method = RequestMethod.GET)
    @ResponseBody
    public List<Salle> getAllSalles() {
        return salleService.findAllSalles();
    }

    @RequestMapping(value = "/salles/{lieuName}/", method = RequestMethod.GET)
    @ResponseBody
    public List<Salle> getAllSallesByLieu(@PathVariable(value = "lieuName") String lieuName) {
        log.info("GET ALL SallesByLieu # Lieu " + lieuName);
        Lieu lieu = lieuService.findOneByName(lieuName);
        return salleService.findAllSallesByLieu(lieu);
    }

    @RequestMapping(value = "/salle", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void updateSalle(@RequestBody final Salle salle) {
        salleService.update(salle);
    }

    @RequestMapping(value = "/salle/indisponible", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void setIndisponible(@RequestBody final Salle salle) {
        audit.record("Mise à jour de la salle (indisponibilité) " + ReflectionToStringBuilder.reflectionToString(salle));
        salleService.updateDisponibilite(salle);
    }

}
