package fr.opale.web.controller;

import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.FormateurService;
import fr.opale.spring.security.SecurityUtils;
import fr.opale.web.commons.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 31/08/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class SecurityController {

    @Autowired
    private FormateurService formateurService;

    @Autowired
    private SecurityUtils secu;

    @RequestMapping(value = "/user/account", method = RequestMethod.GET)
    @ResponseBody
    public Formateur getAccount(){
        log.debug("Récupération du user: " + secu.getCurrentLogin());
        Formateur formateur = formateurService.findByUsernameWithHabilitation(secu.getCurrentLogin());
        return formateur;
    }

    @RequestMapping(value = "/users/username", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getUsernames(){
        log.debug("Récupération du user: " + secu.getCurrentLogin());
        List<String> usernames= formateurService.findAllUsername();
        return usernames;
    }

}
