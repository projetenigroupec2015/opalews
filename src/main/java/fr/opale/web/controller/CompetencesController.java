package fr.opale.web.controller;

import fr.opale.persistence.model.Competence;
import fr.opale.persistence.service.CompetenceService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OpaleWS - fr.opale.web.controller
 * <p>
 * Created by kro on 21/10/16.
 * On 21/10/16
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class CompetencesController {

    @Autowired
    private CompetenceService competenceService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/competences", method = RequestMethod.GET)
    @ResponseBody
    public List<Competence> getAllCompetences() {
        return competenceService.findAll();
    }

    @RequestMapping(value = "/competence", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Competence createCompetence(@RequestBody Competence competence){
        audit.record("Création d'une compétence : " + ReflectionToStringBuilder.reflectionToString(competence));
        return competenceService.create(competence);
    }

}
