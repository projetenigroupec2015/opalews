package fr.opale.web.controller;

import fr.opale.persistence.model.Lieu;
import fr.opale.persistence.service.LieuService;
import fr.opale.web.commons.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 15/09/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class LieuController {

    @Autowired
    private LieuService lieuService;

    @RequestMapping(value = "/lieux", method = RequestMethod.GET)
    @ResponseBody
    public List<Lieu> getAllLieux() {
        return lieuService.findAll();
    }
}
