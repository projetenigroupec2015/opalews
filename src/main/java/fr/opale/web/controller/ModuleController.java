package fr.opale.web.controller;

import fr.opale.persistence.model.Module;
import fr.opale.persistence.service.ModuleService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 13/08/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class ModuleController {

    @Autowired
    private ModuleService moduleService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/modules", method = RequestMethod.GET)
    @ResponseBody
    public List<Module> getAllModules() {
        return moduleService.findAll();
    }

    @RequestMapping(value = "/modules/{idModule}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateModule(@RequestBody Module module){
        audit.record("Mise à jour du module " + ReflectionToStringBuilder.reflectionToString(module));
        moduleService.update(module);
    }
}
