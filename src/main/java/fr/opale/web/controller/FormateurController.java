package fr.opale.web.controller;

import com.google.common.base.Preconditions;
import fr.opale.persistence.model.AffectationLieu;
import fr.opale.persistence.model.Cours;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.AffectationLieuService;
import fr.opale.persistence.service.FormateurService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import fr.opale.web.utils.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class FormateurController {

    @Autowired
    private FormateurService formateurService;

    @Autowired
    private AffectationLieuService affectationLieuService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/formateurs/{idFormateur}/affectations", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<AffectationLieu> getAllAffectation(@PathVariable(value = "idFormateur") int idFormateur) {
        Formateur formateur = formateurService.findOne(idFormateur);
        return affectationLieuService.findAllAffectationByIdFormateur(formateur);
    }

    @RequestMapping(value = "/formateurs", method = RequestMethod.GET)
    @ResponseBody
    public List<Formateur> getAllFormateurs() {
        return formateurService.findAll();
    }

    @RequestMapping(value = "/formateurs", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void updateAllFormateur(@RequestBody final List<Formateur> formateurs) {
        log.info("UPDATE AllFormateur");
        for (Formateur formateur : formateurs) {
            Preconditions.checkNotNull(formateur);
            RestPreconditions.checkFound(formateurService.isExisting(formateur.getIdFormateur()));
            formateurService.update(formateur);
        }
    }

    @RequestMapping(value = "/formateur/cours/experiences/{idFormateur}/{idModule}", method = RequestMethod.GET)
    @ResponseBody
    public int findExperiencesByFormateurAndModule(@PathVariable(value = "idFormateur") Integer idFormateur,
                                                  @PathVariable(value = "idModule") Integer idModule) {
        return formateurService.findExperiencesByFormateurAndModule(idFormateur, idModule);
    }

    @RequestMapping(value = "/formateur/cours/{idFormateur}/{dateStart}/{dateEnd}", method = RequestMethod.GET)
    @ResponseBody
    public List<Cours> getAllCoursByFormateur(@PathVariable(value = "idFormateur") Integer idFormateur,
                                              @PathVariable(value = "dateStart") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
                                              @PathVariable(value = "dateEnd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd) {
        return formateurService.findAllCoursByFormateur(idFormateur,dateStart, dateEnd);
    }

    @RequestMapping(value = "/formateur/deplacementsExterne/{idFormateur}", method = RequestMethod.GET)
    @ResponseBody
    public List<AffectationLieu> findDeplacementsExterieurFormateur(@PathVariable(value = "idFormateur") Integer idFormateur){
        return formateurService.findDeplacementsExterieurFormateur(idFormateur);
    }

    @RequestMapping(value = "/formateur", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateFormateur(@RequestBody final Formateur formateur) {
        log.info("UPDATE Formateur : " + ReflectionToStringBuilder.toString(formateur));
        Preconditions.checkNotNull(formateur);
        RestPreconditions.checkFound(formateurService.isExisting(formateur.getIdFormateur()));
        formateurService.update(formateur);
    }

    @RequestMapping(value = "/formateurs/{idFormateur}/habilitation", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateHabilitationFormateur(@RequestBody final Formateur formateur) {
        audit.record("Mise à jour de l'habilitation : " + ReflectionToStringBuilder.reflectionToString(formateur));
        formateurService.updateHabilitation(formateur);
    }

    @RequestMapping(value = "/formateurs/{idFormateur}/competences", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void updateCompetence(@RequestBody Formateur formateur) {

        formateurService.update(formateur);
    }

    @RequestMapping(value = "/formateurs/{idFormateur}/affectation", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void updateAffectation(@RequestBody Formateur formateur) {

        formateurService.update(formateur);
    }

}
