package fr.opale.web.controller;

import fr.opale.persistence.model.Cours;
import fr.opale.persistence.model.Ecf;
import fr.opale.persistence.service.CoursService;
import fr.opale.persistence.service.EcfService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by JEREMY on 15/07/2016.
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class CoursController {

    @Autowired
    private CoursService coursService;

    @Autowired
    private EcfService ecfService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/planning/{dateStart}/{dateEnd}", method = RequestMethod.GET)
    @ResponseBody
    public List<Cours> getAllCours(@PathVariable(value = "dateStart") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
                                   @PathVariable(value = "dateEnd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd) {
        return coursService.findAllCours(dateStart, dateEnd);
    }

    @RequestMapping(value = "/cours", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateCours(@RequestBody Cours cours)
    {
        audit.record("Mise à jour du cours " + cours.getLibelle());
        coursService.updateCours(cours);
        Ecf ecfUpdated = cours.getEcf();
        ecfUpdated.setCours(cours);
        ecfService.update(ecfUpdated);

    }
}
