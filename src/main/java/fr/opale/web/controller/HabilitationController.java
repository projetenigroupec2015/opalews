package fr.opale.web.controller;

import fr.opale.persistence.model.Habilitation;
import fr.opale.persistence.service.HabilitationService;
import fr.opale.web.commons.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 26/09/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class HabilitationController {

    @Autowired
    private HabilitationService habilitationService;

    @RequestMapping(value = "/habilitations")
    @ResponseBody
    public List<Habilitation> getHabilitations(){
        return habilitationService.findAll();
    }
}
