package fr.opale.web.controller;

import fr.opale.persistence.model.Configuration;
import fr.opale.persistence.model.Parametrage;
import fr.opale.persistence.service.ParametrageService;
import fr.opale.web.commons.Constants;
import fr.opale.web.utils.Audit;
import fr.opale.web.utils.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 15/09/16
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class ParametrageController {

    @Autowired
    private ParametrageService parametrageService;

    @Autowired
    private Audit audit;

    @RequestMapping(value = "/alertes", method = RequestMethod.GET)
    @ResponseBody
    public List<Parametrage> getAllAlertes() {
        return parametrageService.findAllAlertes();
    }

    @RequestMapping(value = "/alerte/{codeAlerte}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void updateAlerte(@PathVariable String codeAlerte, @RequestBody final Configuration configuration) {
        parametrageService.updateConfiguration(configuration.getParametre().getCode(), configuration.getValeur());
    }

    //PARAMETRAGE

    @RequestMapping(value = "/parametrages", method = RequestMethod.GET)
    @ResponseBody
    public List<Parametrage> getAllParams() {
        return parametrageService.findAllParams();
    }

    @RequestMapping(value = "/parametrage/{codeParam}", method = RequestMethod.GET)
    @ResponseBody
    public Parametrage getParamByCode(@PathVariable String codeParam) {
        log.info("GET ParamByCode # id : " + codeParam);
        return RestPreconditions.checkFound(parametrageService.findOne(codeParam));
    }

    @RequestMapping(value = "/parametrage/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateParams(@PathVariable String id, @RequestBody final Parametrage param) {
        parametrageService.update(param);
    }
}
