package fr.opale.web.controller;

import com.google.common.base.Preconditions;
import fr.opale.persistence.model.Activite;
import fr.opale.persistence.model.FicheTemps;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.model.TypeActivite;
import fr.opale.persistence.service.ActiviteService;
import fr.opale.persistence.service.ActiviteTypeService;
import fr.opale.persistence.service.FicheTempsService;
import fr.opale.persistence.service.FormateurService;
import fr.opale.web.exception.IncompatibleDateException;
import fr.opale.web.exception.IncompatibleObjectAndIdException;
import fr.opale.web.exception.ResourceNotFoundException;
import fr.opale.web.utils.Audit;
import fr.opale.web.utils.RestPreconditions;
import fr.opale.web.commons.Constants;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 09/06/2016
 * Package : fr.opale.web.controller
 */
@Slf4j
@Controller
@RequestMapping(value = Constants.API_VERSION)
public class FicheTempsController {

    @Autowired
    private Audit audit;

    @Autowired
    private FicheTempsService ficheTempsService;

    @Autowired
    private FormateurService formateurService;

    @Autowired
    private ActiviteService activiteService;

    @Autowired
    private ActiviteTypeService activiteTypeService;

    @RequestMapping(value = "from/{monthStart}/{yearStart}/to/{monthEnd}/{yearEnd}")
    @ResponseBody
    public Collection<FicheTemps> getFichesTempsByPeriod(
            @PathVariable(value = "monthStart") Integer monthStart,
            @PathVariable(value = "yearStart") Integer yearStart,
            @PathVariable(value = "monthEnd") Integer monthEnd,
            @PathVariable(value = "yearEnd") Integer yearEnd,
            @RequestParam(value = "archived", required = false) Boolean archived,
            @RequestParam(value = "valid", required = false) Boolean valid,
            @RequestParam(value = "formateurId", required = false) Integer formateurId
    ) {
        if (isConsistentDates(monthStart, yearStart, monthEnd, yearEnd)) {
            throw new IncompatibleDateException();
        }

        Formateur formateur = null;
        if (formateurId != null) {
            formateur = RestPreconditions.checkFound(formateurService.findOne(formateurId));
        }

        Collection<FicheTemps> fichesTemps;
        if (valid == null) {
            if (archived == null) {
                if (formateur == null) {
                    fichesTemps = ficheTempsService.findInPeriod(
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                } else {
                    fichesTemps = ficheTempsService.findByFormateurInPeriod(
                            formateur,
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                }
            } else {
                if (formateur == null) {
                    fichesTemps = ficheTempsService.findByArchivedInPeriod(
                            archived,
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                } else {
                    fichesTemps = ficheTempsService.findByFormateurAndArchivedInPeriod(
                            formateur,
                            archived,
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                }
            }
        } else {
            if (archived == null) {
                if (formateur == null) {
                    fichesTemps = ficheTempsService.findByValidInPeriod(
                            valid,
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                } else {
                    fichesTemps = ficheTempsService.findByFormateurAndValidInPeriod(
                            formateur,
                            valid,
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                }
            } else {
                if (formateur == null) {
                    fichesTemps = ficheTempsService.findByArchivedAndValidInPeriod(
                            archived,
                            valid,
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                } else {
                    fichesTemps = ficheTempsService.findByFormateurAndArchivedAndValidInPeriod(
                            formateur,
                            archived,
                            valid,
                            monthStart,
                            yearStart,
                            monthEnd,
                            yearEnd
                    );
                }
            }
        }

//        if (fichesTemps == null) {
//            throw new ResourceNotFoundException();
//        }

        return fichesTemps;
    }

    @RequestMapping(value = "on/{month}/{year}")
    @ResponseBody
    public Collection<FicheTemps> getFichesTempsByPeriod(
            @PathVariable(value = "month") Integer month,
            @PathVariable(value = "year") Integer year,
            @RequestParam(value = "archived", required = false) Boolean archived,
            @RequestParam(value = "valid", required = false) Boolean valid
    ) {
        if (!validMonth(month)) {
//            TODO lever Exception
//            throw new
        }

        Collection<FicheTemps> fichesTemps;
        if (valid == null) {
            if (archived == null) {
                fichesTemps = ficheTempsService.findByDate(
                        month,
                        year
                );
            } else {
                fichesTemps = ficheTempsService.findByArchivedAndDate(
                        archived,
                        month,
                        year
                );
            }
        } else {
            if (archived == null) {
                fichesTemps = ficheTempsService.findByValidAndDate(
                        valid,
                        month,
                        year
                );
            } else {
                fichesTemps = ficheTempsService.findByArchivedAndValidAndDate(
                        archived,
                        valid,
                        month,
                        year
                );
            }
        }

        return fichesTemps;
    }

    @RequestMapping(value = "/ficheTemps/{month}/{year}")
    @ResponseBody
    public FicheTemps getFicheTempsByFormateurAndDate(
            @RequestParam(value = "formateurId") Integer formateurId,
            @PathVariable(value = "month") Integer month,
            @PathVariable(value = "year") Integer year
    ) {
        if (!validMonth(month)) {
//            TODO lever Exception
//            throw new
        }

       Formateur formateur = null;
        if (formateurId != null) {
            formateur = RestPreconditions.checkFound(formateurService.findOne(formateurId));
    }

        return ficheTempsService.findByFormateurAndDate(formateur, month, year);
    }

    private boolean isConsistentDates(Integer monthStart, Integer yearStart, Integer monthEnd, Integer yearEnd) {
        return yearStart < yearEnd || (yearStart.equals(yearEnd) && monthStart <= monthEnd);
    }

    private boolean validMonth(int month) {
        return month > 0 && month < 13;
    }


    @RequestMapping(value = "/activite", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.CREATED)
    public Activite createActivite(@RequestBody Activite activite) {
        audit.record("Création d'une activité " + ReflectionToStringBuilder.reflectionToString(activite));
        activite = activiteService.create(activite);

        return activite;
    }

    @RequestMapping(value = "/activite/{idActivite}", method = RequestMethod.PUT, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.OK)
    public void updateActivite(@PathVariable("idActivite") int idActivite, @RequestBody Activite activite) {
        audit.record("Mise à jour d'une activité " + ReflectionToStringBuilder.reflectionToString(activite));
        Preconditions.checkNotNull(activite);
        RestPreconditions.checkFound(activiteService.findOne(idActivite));
        if (idActivite != activite.getIdActivite()) {
            throw new IncompatibleObjectAndIdException();
        }

        activiteService.update(activite);
    }

    @RequestMapping(value = "/ficheTemps", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.CREATED)
    public FicheTemps createFicheTemps(@RequestBody FicheTemps ficheTemps) {
        audit.record("Création d'une fiche de temps " + ReflectionToStringBuilder.reflectionToString(ficheTemps));
        ficheTemps = ficheTempsService.create(ficheTemps);

        return ficheTemps;
    }

    @RequestMapping(value = "/ficheTemps/{idFicheTemps}", method = RequestMethod.PUT, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.OK)
    public void updateFicheTemps(@PathVariable("idFicheTemps") int idFicheTemps, @RequestBody FicheTemps ficheTemps) {
        audit.record("Mise à jour d'une fiche de temps " + ReflectionToStringBuilder.reflectionToString(ficheTemps));
        Preconditions.checkNotNull(ficheTemps);
        RestPreconditions.checkFound(activiteService.findOne(idFicheTemps));
        if (idFicheTemps != ficheTemps.getIdFicheTemps()) {
            throw new IncompatibleObjectAndIdException();
        }

        ficheTempsService.update(ficheTemps);
    }

    @RequestMapping(value = "/typeActivite", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.CREATED)
    public TypeActivite createTypeActivite(@RequestBody TypeActivite typeActivite) {
        audit.record("Création d'un type d'activité " + ReflectionToStringBuilder.reflectionToString(typeActivite));
        typeActivite = activiteTypeService.create(typeActivite);

        return typeActivite;
    }

    @RequestMapping(value = "/typeActivite/{codeTypeActivite}", method = RequestMethod.PUT, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.OK)
    public void updateTypeActivite(@PathVariable("codeTypeActivite") String codeTypeActivite, @RequestBody TypeActivite typeActivite) {
        audit.record("Mise à jour d'un type d'activité " + ReflectionToStringBuilder.reflectionToString(typeActivite));
        Preconditions.checkNotNull(typeActivite);
        RestPreconditions.checkFound(activiteTypeService.findOne(codeTypeActivite));
        if (!codeTypeActivite.equals(typeActivite.getCodeActivite())) {
            throw new IncompatibleObjectAndIdException();
        }

        activiteTypeService.update(typeActivite);
    }
}
