package fr.opale.web.controller;

import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.model.Notification;
import fr.opale.persistence.service.FormateurService;
import fr.opale.persistence.service.NotificationService;
import fr.opale.web.commons.Constants;
import fr.opale.web.exception.InvalideMethodParameterException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 23/10/2016
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private FormateurService formateurService;

    @RequestMapping(value = "/notifications", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Notification> getAllNotifications() {
        return notificationService.getAllNotification();
    }

    @RequestMapping(value = "/formateur/{formateurId}/notifications", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Notification> getNotificationsByFormateur(@PathVariable int formateurId) {
        Formateur formateur = formateurService.findOne(formateurId);

        if (formateur == null) {
            throw new InvalideMethodParameterException("L'ID fourni ne correspond à aucun formateur.");
        }

        if (formateur.getHabilitation() != null && "admin".equals(formateur.getHabilitation().getLibelle())) {
            return notificationService.getAllNotification();
        } else {
            return notificationService.getNotificationByFormateur(formateur);
        }
    }
}
