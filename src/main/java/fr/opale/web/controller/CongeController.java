package fr.opale.web.controller;

import com.google.common.base.Preconditions;
import fr.opale.enumeration.EtatCongeEnum;
import fr.opale.enumeration.EtatCongeRequestEnum;
import fr.opale.persistence.model.Conge;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.CongeService;
import fr.opale.persistence.service.FormateurService;
import fr.opale.persistence.service.NotificationService;
import fr.opale.web.commons.Constants;
import fr.opale.web.exception.IncompatibleDateException;
import fr.opale.web.exception.IncompatibleObjectAndIdException;
import fr.opale.web.exception.IncorrectEnumerationValueException;
import fr.opale.web.exception.ResourceNotFoundException;
import fr.opale.web.exception.conge.CongeIncompatibleException;
import fr.opale.web.utils.Audit;
import fr.opale.web.utils.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 09/06/2016
 * Package : fr.opale.web.controller
 */
@Slf4j
@RestController
@RequestMapping(value = Constants.API_VERSION)
public class CongeController {

    @Autowired
    private Audit audit;

    @Autowired
    private CongeService congeService;

    @Autowired
    private FormateurService formateurService;

    @Autowired
    private NotificationService notificationService;


    @RequestMapping(value = "/conges/{congeId}", method = RequestMethod.GET)
    @ResponseBody
    public Conge getCongeById(@PathVariable int congeId) {
        log.info("GET CongeById # id : " + congeId);
        Conge conge = congeService.findOne(congeId);
        if (conge == null) {
            throw new ResourceNotFoundException();
        }
        return conge;
    }

    @RequestMapping(value = "/conges/{dateStart}/{dateEnd}", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Conge> getCongeInPeriod4Formateur(
            @PathVariable(value = "dateStart") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
            @PathVariable(value = "dateEnd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd,
            @RequestParam(value = "etat", required = false) String etat,
            @RequestParam(value = "formateurId", required = false) Integer formateurId
    ) {
        if (dateStart.isAfter(dateEnd)) {
            throw new IncompatibleDateException();
        }

        etat = "null".equals(etat)?null:etat;

        Formateur formateur = null;
        if (formateurId != null) {
            formateur = RestPreconditions.checkFound(formateurService.findOne(formateurId));
        }

        Collection<Conge> conges;
        if (etat == null) {
            if (formateur == null) {
                conges = congeService.findInPeriod(dateStart, dateEnd);
            } else {
                conges = congeService.findByFormateurInPeriod(formateur, dateStart, dateEnd);
            }
        } else {
            EtatCongeRequestEnum etatRequest = EtatCongeRequestEnum.getEtatCongeRequeteEnum(etat);
            if (formateur == null) {
                if (etatRequest != null) {
                    switch (etatRequest) {
                        case ALL:
                            conges = congeService.findInPeriod(dateStart, dateEnd);
                            break;
                        case ACTIF:
                            conges = congeService.findActiveInPeriod(dateStart, dateEnd);
                            break;
                        default:
                            throw new IncorrectEnumerationValueException();
                    }
                } else {
                    EtatCongeEnum etatConge = EtatCongeEnum.getEnum(etat);
                    if (etatConge == null) {
                        throw new IncorrectEnumerationValueException();
                    }

                    switch (etatConge) {
                        case EN_ATTENTE:
                        case VALIDE:
                        case REFUSE:
                            conges = congeService.findByEtatCongeInPeriod(etatConge, dateStart, dateEnd);
                            break;
                        default:
                            throw new IncorrectEnumerationValueException();
                    }
                }
            } else {
                if (etatRequest != null) {
                    switch (etatRequest) {
                        case ALL:
                            conges = congeService.findByFormateurInPeriod(formateur, dateStart, dateEnd);
                            break;
                        case ACTIF:
                            conges = congeService.findActiveByFormateurInPeriod(formateur, dateStart, dateEnd);
                            break;
                        default:
                            throw new IncorrectEnumerationValueException();
                    }
                } else {
                    EtatCongeEnum etatConge = EtatCongeEnum.getEnum(etat);
                    if (etatConge == null) {
                        throw new IncorrectEnumerationValueException();
                    }

                    switch (etatConge) {
                        case EN_ATTENTE:
                        case VALIDE:
                        case REFUSE:
                            conges = congeService.findByFormateurAndEtatCongeInPeriod(
                                    formateur,
                                    etatConge,
                                    dateStart,
                                    dateEnd
                            );
                            break;
                        default:
                            throw new IncorrectEnumerationValueException();
                    }
                }
            }
        }

//        if (conges == null || conges.size() == 0) {
//            throw new ResourceNotFoundException();
//        }

        return conges;
    }

    @RequestMapping(value = "/conge", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.CREATED)
    public Conge createConge(@RequestBody Conge conge) {
        audit.record("Création du congé " + ReflectionToStringBuilder.reflectionToString(conge));
        if (!congeService.isValidConge(conge)) {
            throw new CongeIncompatibleException();
        }
        conge.setEtatConge(EtatCongeEnum.EN_ATTENTE);
        conge.setDateDemandeConge(LocalDateTime.now());
        conge = congeService.create(conge);

        notificationService.createNotification(conge);

        return conge;
    }

    @RequestMapping(value = "/conge/{idConge}", method = RequestMethod.PUT, consumes = "application/json; charset=UTF-8")
    @ResponseStatus(HttpStatus.OK)
    public void updateConge(@PathVariable("idConge") int idConge, @RequestBody Conge conge) {
        audit.record("Mise à jour du congé " + ReflectionToStringBuilder.reflectionToString(conge));
        Preconditions.checkNotNull(conge);
        RestPreconditions.checkFound(congeService.findOne(idConge));
        if (idConge != conge.getIdConge()) {
            throw new IncompatibleObjectAndIdException();
        }

        if (conge.getEtatConge() != EtatCongeEnum.REFUSE) {
            if (!congeService.isValidConge(conge)) {
                throw new CongeIncompatibleException();
            }
        }

        congeService.update(conge);

        if (conge.getEtatConge() != EtatCongeEnum.EN_ATTENTE) {
            notificationService.supprimerNotification(conge);
        }
    }
}
