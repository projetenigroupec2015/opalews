package fr.opale.persistence.etl;

import fr.opale.persistence.etl.talendfile.loaddata_1_1.LoadData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Thomas Croguennec
 * Date : 30/07/16
 * Package : fr.opale.persistence
 */
public class ETLProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(ETLProcess.class);

    public void run(){
        LoadData job = new LoadData();
        LOGGER.debug("Lancement du job...");
        job.runJob(new String[]{});
        LOGGER.debug("Job State:"+ job.getStatus());
        LOGGER.debug("Fin du Job");
    }

}
