package fr.opale.persistence.dao;

import fr.opale.persistence.model.Promotion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by JEREMY on 14/08/2016.
 */
public interface PromotionDao extends CrudRepository<Promotion, String> {

    @Query("SELECT p FROM Promotion p " +
            "LEFT JOIN FETCH p.lieu l " +
            "LEFT JOIN FETCH p.formation f " +
            "WHERE p.fin > :dateStart " +
            "AND p.debut < :dateEnd")

    public List<Promotion> findAllPromotions(@Param("dateStart")LocalDateTime dateStart,
                                             @Param("dateEnd") LocalDateTime dateEnd);

    static final String FIND_PROMOTIONS_BY_INTERVAL_DATE = "SELECT p FROM Promotion p";
}
