package fr.opale.persistence.dao;

import fr.opale.persistence.model.AffectationLieu;
import fr.opale.persistence.model.Formateur;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * OpaleWS - fr.opale.persistence.dao
 * <p>
 * Created by Thomas Croguennec on 22/10/16.
 * On 22/10/16
 */
public interface AffectationLieuDao extends CrudRepository<AffectationLieu, Integer> {

    @Query("SELECT a FROM AffectationLieu a WHERE a.formateur=:formateur")
    public List<AffectationLieu> findAllAffectationByIdFormateur(@Param("formateur") Formateur formateur);
}
