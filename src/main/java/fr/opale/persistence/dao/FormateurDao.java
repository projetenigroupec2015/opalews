package fr.opale.persistence.dao;

import fr.opale.persistence.model.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : ${PACKAGE_NAME}
 */
public interface FormateurDao extends PagingAndSortingRepository<Formateur, Integer> {

    static final String SELECT = "SELECT f FROM Formateur f ";

    static final String FIND_DEPLACEMENT_BY_FORMATEUR = "SELECT affectation FROM AffectationLieu affectation " +
            "INNER JOIN FETCH affectation.lieu l " +
            "where affectation.preferenceLieu > ( " +
            "select affec.preferenceLieu " +
            "from AffectationLieu affec " +
            "LEFT JOIN affec.formateur fo " +
            "where fo.idFormateur = :idFormateur " +
            "and affec.isDefault=1) ";

    static final String FIND_COUNT_COURS_BY_FORMATEUR_AND_MODULE = "SELECT COUNT(m) FROM Cours c " +
            "INNER JOIN c.formateur fo " +
            "INNER JOIN c.module m " +
            "WHERE fo.idFormateur = :idFormateur " +
            "AND m.id = :idModule " +
            "GROUP BY m";

    static final String FIND_COURS_BY_INTERVAL_DATE_AND_FORMATEUR = "SELECT c FROM Cours c " +
            "INNER JOIN FETCH c.promotion p " +
            "INNER JOIN FETCH c.formateur fo " +
            "INNER JOIN FETCH c.module m " +
            "LEFT JOIN FETCH c.salle s " +
            "LEFT JOIN FETCH s.lieu l " +
            "WHERE fo.idFormateur = :idFormateur " +
            "AND c.fin > :dateStart " +
            "AND c.debut < :dateEnd " +
            "ORDER BY c.debut";

    @Query(SELECT + " JOIN FETCH f.habilitation h WHERE f.username = ?1")
    public Formateur findByUsernameWithHabilitation(String login);

    @Query(SELECT + " WHERE f.username = ?1")
    public Formateur findByUsername(String login);

    @Query("SELECT f FROM Formateur f  " +
           "LEFT JOIN FETCH f.conges " +
           "WHERE f.mail <> ''")
    public List<Formateur> findAllFormateur();

    @Query("SELECT f.username FROM Formateur f")
    public List<String> findAllUsername();

    @Query(FIND_COUNT_COURS_BY_FORMATEUR_AND_MODULE)
    public int findCountCoursByFormateurAndModule(@Param("idFormateur") Integer idFormateur,
                                                  @Param("idModule") Integer idModule);
    @Query(FIND_COURS_BY_INTERVAL_DATE_AND_FORMATEUR)
    public List<Cours> findAllCoursByFormateur(@Param("idFormateur") Integer idFormateur,
                                               @Param("dateStart") LocalDateTime dateStart,
                                               @Param("dateEnd") LocalDateTime dateEnd);

    @Query(FIND_DEPLACEMENT_BY_FORMATEUR)
    public List<AffectationLieu> findDeplacementsExterieurFormateur(@Param("idFormateur") Integer idFormateur);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE Formateur f SET f.habilitation=:habilitation WHERE f.idFormateur=:idFormateur")
    public int updateHabilitation(@Param(value = "habilitation") Habilitation habilitation, @Param(value = "idFormateur") int idFormateur);

}
