package fr.opale.persistence.dao;

import fr.opale.persistence.model.Formation;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by JEREMY on 26/07/2016.
 */
public interface FormationDao extends PagingAndSortingRepository<Formation, String> {

}
