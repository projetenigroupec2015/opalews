package fr.opale.persistence.dao;

import fr.opale.persistence.model.PisteAudit;
import org.springframework.data.repository.CrudRepository;

/**
 * OpaleWS - fr.opale.persistence.dao
 * <p>
 * Created by Thomas Croguennec on 22/10/16.
 * On 22/10/16
 */
public interface PisteAuditDao extends CrudRepository<PisteAudit, Integer> {
}
