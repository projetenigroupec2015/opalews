package fr.opale.persistence.dao;

import fr.opale.persistence.model.Lieu;
import fr.opale.persistence.model.Parametrage;
import fr.opale.persistence.model.Salle;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 13/07/16
 * Package : fr.opale.persistence.dao
 */
public interface SalleDao extends CrudRepository<Salle, String> {

    static final String SELECT_SALLE_BY_LIEU = "SELECT s FROM Salle s WHERE s.lieu = :idlieu";
    static final String SELECT_SALLE = "SELECT s FROM Salle s LEFT JOIN FETCH s.lieu l";

    @Query(SELECT_SALLE)
    public List<Salle> findAllSalles();

    @Query(SELECT_SALLE_BY_LIEU)
    public List<Salle> findAllSallesByLieu(@Param("idlieu") Lieu idlieu);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE Salle s SET s.disponibilite = :dispo where s.code = :idSalle")
    public int updateDisponibilite(@Param("idSalle") String idSalle, @Param("dispo") boolean dispo);
}
