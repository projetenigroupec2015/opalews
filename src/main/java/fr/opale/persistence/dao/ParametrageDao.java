package fr.opale.persistence.dao;

import fr.opale.persistence.model.Parametrage;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : ${PACKAGE_NAME}
 */
@Repository
@Transactional(noRollbackFor = Exception.class)
public interface ParametrageDao extends CrudRepository<Parametrage, String> {

    @Query("SELECT p FROM Parametrage p WHERE p.type='alerte'")
    public List<Parametrage> findAllAlertes();

    @Query("SELECT p FROM Parametrage p WHERE p.type='configuration'")
    public List<Parametrage> findAllParams();

    @Modifying(clearAutomatically = true)
    @Query("UPDATE Configuration c SET c.valeur=?1 where c.parametre.code = ?2")
    public int saveConfiguration(String codeParam, String value);


}
