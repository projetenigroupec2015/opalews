package fr.opale.persistence.dao;

import fr.opale.persistence.model.Lieu;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 13/07/16
 * Package : fr.opale.persistence.dao
 */
public interface LieuDao extends CrudRepository<Lieu, Integer> {

    static final String SELECT_LIEU_BY_NAME = "SELECT l FROM Lieu l WHERE  l.libelle= :lieuName";

    @Query(SELECT_LIEU_BY_NAME)
    public Lieu findOneByName(@Param("lieuName") String lieuName);
}
