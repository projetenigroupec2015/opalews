package fr.opale.persistence.dao;

import fr.opale.persistence.model.Competence;
import org.springframework.data.repository.CrudRepository;

/**
 * OpaleWS - fr.opale.persistence.dao
 * <p>
 * Created by kro on 21/10/16.
 * On 21/10/16
 */
public interface CompetenceDao extends CrudRepository<Competence, String> {
}
