package fr.opale.persistence.dao;

import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.model.Notification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 23/10/2016
 * Package : fr.opale.persistence.dao
 */
public interface NotificationDao extends CrudRepository<Notification, Integer> {

    static final String BASE_REQUEST = "SELECT DISTINCT n" +
            " FROM Notification n" +
            " LEFT JOIN FETCH n.formateur f" +
            " LEFT JOIN FETCH n.module" +
            " LEFT JOIN FETCH n.parametre" +
            " WHERE 1 = 1";

    static final String WHERE_MESSAGE = " AND n.message = :message";
    static final String WHERE_FORATEUR = " AND n.formateur = :formateur";

    static final String FIND_ALL = BASE_REQUEST;
    static final String FIND_BY_FORMATEUR_AND_MESSAGE = BASE_REQUEST + WHERE_FORATEUR + WHERE_MESSAGE;
    static final String FIND_BY_FORMATEUR = BASE_REQUEST + WHERE_FORATEUR;

    @Query(FIND_ALL)
    public Collection<Notification> getAllNotification();

    @Query(FIND_BY_FORMATEUR_AND_MESSAGE)
    public Notification getNotificationByFormateurAndMessage(
            @Param("formateur") Formateur formateur,
            @Param("message") String message
    );

    @Query(FIND_BY_FORMATEUR)
    public Collection<Notification> getNotificationByFormateur (
            @Param("formateur") Formateur formateur
    );
}
