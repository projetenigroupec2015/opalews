package fr.opale.persistence.dao;

import fr.opale.persistence.model.FicheTemps;
import fr.opale.persistence.model.Formateur;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.dao
 */
public interface FicheTempsDao extends PagingAndSortingRepository<FicheTemps, Integer> {

    static final String BASE_REQUEST = "SELECT DISTINCT ft FROM FicheTemps ft WHERE 1=1";

    static final String WHERE_IN_PERIOD = " AND (" +
            "(:yearStart = :yearEnd AND moisFicheTemps >= :monthStart AND moisFicheTemps <= :monthEnd) " +
            " OR (anneeFicheTemps = :yearStart AND anneeFicheTemps < :yearEnd AND moisFicheTemps >= :monthStart)" +
            " OR (anneeFicheTemps = :yearEnd AND anneeFicheTemps > :yearStart AND moisFicheTemps <= :monthEnd)" +
            " OR (anneeFicheTemps > :yearStart AND anneeFicheTemps < :yearEnd)" +
            ")";
    static final String WHERE_BY_FORMATEUR = " AND formateur = :formateur";
    static final String WHERE_BY_DATE = " AND anneeFicheTemps = :year AND moisFicheTemps = :month";
    static final String WHERE_BY_VALID_STATE = " AND valide = :valid";
    static final String WHERE_BY_ARCHIVED_STATE = " AND archive = :archived";

    static final String FIND_IN_PERIOD = BASE_REQUEST + WHERE_IN_PERIOD;
    static final String FIND_BY_FORMATEUR_IN_PERIOD = BASE_REQUEST + WHERE_BY_FORMATEUR + WHERE_IN_PERIOD;
    static final String FIND_BY_DATE = BASE_REQUEST + WHERE_BY_DATE;
    static final String FIND_BY_FORMATEUR_AND_DATE = BASE_REQUEST + WHERE_BY_FORMATEUR + WHERE_BY_DATE;
    static final String FIND_BY_ARCHIVE_IN_PERIOD = BASE_REQUEST + WHERE_BY_ARCHIVED_STATE + WHERE_IN_PERIOD;
    static final String FIND_BY_FORMATEUR_AND_ARCHIVE_IN_PERIOD = BASE_REQUEST + WHERE_BY_FORMATEUR + WHERE_BY_ARCHIVED_STATE + WHERE_IN_PERIOD;
    static final String FIND_BY_ARCHIVE_AND_DATE = BASE_REQUEST + WHERE_BY_ARCHIVED_STATE + WHERE_BY_DATE;
    static final String FIND_BY_VALIDE_IN_PERIOD = BASE_REQUEST + WHERE_BY_VALID_STATE + WHERE_IN_PERIOD;
    static final String FIND_BY_FORMATEUR_AND_VALIDE_IN_PERIOD = BASE_REQUEST + WHERE_BY_FORMATEUR + WHERE_BY_VALID_STATE + WHERE_IN_PERIOD;
    static final String FIND_BY_VALIDE_AND_DATE = BASE_REQUEST + WHERE_BY_VALID_STATE + WHERE_BY_DATE;
    static final String FIND_BY_ARCHIVE_AND_VALIDE_IN_PERIOD = BASE_REQUEST + WHERE_BY_ARCHIVED_STATE + WHERE_BY_VALID_STATE + WHERE_IN_PERIOD;
    static final String FIND_BY_FORMATEUR_AND_ARCHIVE_AND_VALIDE_IN_PERIOD = BASE_REQUEST + WHERE_BY_FORMATEUR + WHERE_BY_ARCHIVED_STATE + WHERE_BY_VALID_STATE + WHERE_IN_PERIOD;
    static final String FIND_BY_ARCHIVE_AND_VALIDE_AND_DATE = BASE_REQUEST + WHERE_BY_ARCHIVED_STATE + WHERE_BY_VALID_STATE + WHERE_BY_DATE;


    @Query(FIND_IN_PERIOD)
    Collection<FicheTemps> findInPeriod(
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_ARCHIVE_IN_PERIOD)
    Collection<FicheTemps> findByArchivedInPeriod(
            @Param("archived") boolean archived,
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_FORMATEUR_IN_PERIOD)
    Collection<FicheTemps> findByFormateurInPeriod(
            @Param("formateur") Formateur formateur,
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_FORMATEUR_AND_ARCHIVE_IN_PERIOD)
    Collection<FicheTemps> findByFormateurAndArchivedInPeriod(
            @Param("formateur") Formateur formateur,
            @Param("archived") boolean archived,
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_DATE)
    Collection<FicheTemps> findByDate(
            @Param("month") Integer month,
            @Param("year") Integer year
    );

    @Query(FIND_BY_ARCHIVE_AND_DATE)
    Collection<FicheTemps> findByArchivedAndDate(
            @Param("archived") boolean archived,
            @Param("month") Integer month,
            @Param("year") Integer year
    );

    @Query(FIND_BY_FORMATEUR_AND_DATE)
    FicheTemps findByFormateurAndDate(
            @Param("formateur") Formateur formateur,
            @Param("month") Integer month,
            @Param("year") Integer year
    );

    @Query(FIND_BY_VALIDE_IN_PERIOD)
    Collection<FicheTemps> findByValidInPeriod(
            @Param("valid") boolean valid,
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_ARCHIVE_AND_VALIDE_IN_PERIOD)
    Collection<FicheTemps> findByArchivedAndValidInPeriod(
            @Param("archived") boolean archived,
            @Param("valid") boolean valid,
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_FORMATEUR_AND_VALIDE_IN_PERIOD)
    Collection<FicheTemps> findByFormateurAndValidInPeriod(
            @Param("formateur") Formateur formateur,
            @Param("valid") boolean valid,
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_FORMATEUR_AND_ARCHIVE_AND_VALIDE_IN_PERIOD)
    Collection<FicheTemps> findByFormateurAndArchivedAndValidInPeriod(
            @Param("formateur") Formateur formateur,
            @Param("archived") boolean archived,
            @Param("valid") boolean valid,
            @Param("monthStart")Integer monthStart,
            @Param("yearStart")Integer yearStart,
            @Param("monthEnd") Integer monthEnd,
            @Param("yearEnd") Integer yearEnd
    );

    @Query(FIND_BY_VALIDE_AND_DATE)
    Collection<FicheTemps> findByValidAndDate(
            @Param("valid") boolean valid,
            @Param("month") Integer month,
            @Param("year") Integer year
    );

    @Query(FIND_BY_ARCHIVE_AND_VALIDE_AND_DATE)
    Collection<FicheTemps> findByArchivedAndValidAndDate(
            @Param("archived") boolean archived,
            @Param("valid") boolean valid,
            @Param("month") Integer month,
            @Param("year") Integer year
    );
}
