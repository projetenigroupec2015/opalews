package fr.opale.persistence.dao;

import fr.opale.persistence.model.Ecf;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.dao
 */
public interface EcfDao extends CrudRepository<Ecf, Integer> {
}
