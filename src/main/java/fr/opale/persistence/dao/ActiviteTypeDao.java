package fr.opale.persistence.dao;

import fr.opale.persistence.model.TypeActivite;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.dao
 */
public interface ActiviteTypeDao extends PagingAndSortingRepository<TypeActivite, String> {

}
