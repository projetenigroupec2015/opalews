package fr.opale.persistence.dao;

import fr.opale.persistence.model.Cours;
import fr.opale.persistence.model.Ecf;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.model.Salle;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by JEREMY on 15/07/2016.
 */
public interface CoursDao extends CrudRepository<Cours, Integer> {

    static final String FIND_COURS_BY_INTERVAL_DATE = "SELECT c FROM Cours c  " +
            "INNER JOIN FETCH c.promotion p " +
            "INNER JOIN FETCH p.formation f " +
            "INNER JOIN FETCH c.module m " +
            "LEFT JOIN FETCH c.formateur fo " +
            "LEFT JOIN FETCH c.ecf e " +
            "LEFT JOIN FETCH c.salle s " +
            "LEFT JOIN FETCH p.lieu " +
            "LEFT JOIN FETCH s.lieu l " +
            "WHERE c.fin > :dateStart " +
            "AND c.debut < :dateEnd " +
            "ORDER BY c.debut, m.dureeEnSemaines DESC";

    static final String FIND_COURS_BY_INTERVAL_DATE_AND_FORMATEUR = "SELECT c FROM Cours c " +
            "INNER JOIN FETCH c.promotion p " +
            "INNER JOIN FETCH c.formateur fo " +
            "WHERE fo.idFormateur = :idFormateur " +
            "AND c.fin > :dateStart " +
            "AND c.debut < :dateEnd";

    @Query(FIND_COURS_BY_INTERVAL_DATE)
    public List<Cours> findAllCours(@Param("dateStart") LocalDateTime dateStart,
                                    @Param("dateEnd") LocalDateTime dateEnd);

    @Query(FIND_COURS_BY_INTERVAL_DATE_AND_FORMATEUR)
    public List<Cours> findAllCoursByFormateur(@Param("idFormateur") Integer idFormateur,
                                               @Param("dateStart") LocalDateTime dateStart,
                                               @Param("dateEnd") LocalDateTime dateEnd);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE Cours c SET c.salle = :salle " +
            ",c.formateur = :formateur " +
            ",c.ecf = :ecf " +
            " where c.id = :idCours")
    public void updateCours(@Param("idCours") String idCours,
                            @Param("ecf") Ecf ecf,
                            @Param("salle") Salle salle,
                            @Param("formateur") Formateur formateur);
}
