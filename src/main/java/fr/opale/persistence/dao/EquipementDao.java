package fr.opale.persistence.dao;

import fr.opale.persistence.model.Equipement;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Thomas Croguennec
 * Date : 16/07/16
 * Package : fr.opale.persistence.dao
 */
public interface EquipementDao extends CrudRepository<Equipement, Integer> {
}
