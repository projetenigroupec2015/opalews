package fr.opale.persistence.dao;

import fr.opale.enumeration.EtatCongeEnum;
import fr.opale.persistence.model.Conge;
import fr.opale.persistence.model.Formateur;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.dao
 */
public interface CongeDao extends PagingAndSortingRepository<Conge, Integer> {

        static final String BASE_REQUEST = "SELECT DISTINCT c FROM Conge c INNER JOIN FETCH c.formateur f WHERE 1 = 1";

        static final String WHERE_FORMATEUR = " AND c.formateur = :formateur";
        static final String WHERE_IN_PERIOD = " AND c.dateDebutConge < :dateEnd AND c.dateFinConge > :dateStart";
        static final String WHERE_ACTIVE = " AND c.etatConge <> fr.opale.enumeration.EtatCongeEnum.REFUSE";
        static final String WHERE_ETAT_CONGE = " AND c.etatConge = :etatConge";

        static final String FIND_IN_PERIOD = BASE_REQUEST + WHERE_IN_PERIOD;
        static final String FIND_ACTIVE_IN_PERIOD = BASE_REQUEST + WHERE_IN_PERIOD + WHERE_ACTIVE;
        static final String FIND_BY_FORMATEUR_IN_PERIOD = BASE_REQUEST + WHERE_IN_PERIOD + WHERE_FORMATEUR;
        static final String FIND_BY_ETAT_CONGE_IN_PERIOD = BASE_REQUEST + WHERE_IN_PERIOD + WHERE_ETAT_CONGE;
        static final String FIND_BY_FORMATEUR_AND_ETAT_CONGE_IN_PERIOD = BASE_REQUEST + WHERE_IN_PERIOD + WHERE_FORMATEUR + WHERE_ETAT_CONGE;
        static final String FIND_ACTIVE_BY_FORMATEUR_IN_PERIOD = BASE_REQUEST + WHERE_IN_PERIOD + WHERE_FORMATEUR + WHERE_ACTIVE;


        @Query(FIND_IN_PERIOD)
        Collection<Conge> findInPeriod(
                @Param("dateStart") LocalDateTime dateStart,
                @Param("dateEnd") LocalDateTime dateEnd
        );

        @Query(FIND_ACTIVE_IN_PERIOD)
        Collection<Conge> findActiveInPeriod(
                @Param("dateStart") LocalDateTime dateStart,
                @Param("dateEnd") LocalDateTime dateEnd
        );

        @Query(FIND_BY_FORMATEUR_IN_PERIOD)
        Collection<Conge> findByFormateurInPeriod(
                @Param("formateur") Formateur formateur,
                @Param("dateStart") LocalDateTime dateStart,
                @Param("dateEnd") LocalDateTime dateEnd
        );

        @Query(FIND_ACTIVE_BY_FORMATEUR_IN_PERIOD)
        Collection<Conge> findActiveByFormateurInPeriod(
                @Param("formateur") Formateur formateur,
                @Param("dateStart") LocalDateTime dateStart,
                @Param("dateEnd") LocalDateTime dateEnd
        );

        @Query(FIND_BY_ETAT_CONGE_IN_PERIOD)
        Collection<Conge> findByEtatCongeInPeriod(
                @Param("etatConge") EtatCongeEnum etatConge,
                @Param("dateStart") LocalDateTime dateStart,
                @Param("dateEnd") LocalDateTime dateEnd
        );

        @Query(FIND_BY_FORMATEUR_AND_ETAT_CONGE_IN_PERIOD)
        Collection<Conge> findByFormateurAndEtatCongeInPeriod(
                @Param("formateur") Formateur formateur,
                @Param("etatConge") EtatCongeEnum etatConge,
                @Param("dateStart") LocalDateTime dateStart,
                @Param("dateEnd") LocalDateTime dateEnd
        );
}
