package fr.opale.persistence.dao;

import fr.opale.persistence.model.Habilitation;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Brice BRUNEAU
 * Date : 21/06/2016
 * Package : fr.opale.persistence.dao
 */
public interface HabilitationDao extends PagingAndSortingRepository<Habilitation, Integer> {

}
