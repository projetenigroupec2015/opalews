package fr.opale.persistence.dao;

import fr.opale.persistence.model.Competence;
import fr.opale.persistence.model.Equipement;
import fr.opale.persistence.model.Module;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 13/08/16
 * Package : fr.opale.persistence.dao
 */
public interface ModuleDao extends CrudRepository<Module, Integer> {

    @Query("SELECT m.competences FROM Module m WHERE m.id=:idModule")
    public List<Competence> findCompetenceByModule(@Param("idModule") int idModule);

    @Query("SELECT m.equipements FROM Module m WHERE m.id=:idModule")
    public List<Equipement> findEquipementsByModule(@Param("idModule") int idModule);
}
