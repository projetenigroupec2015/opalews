package fr.opale.persistence.service.abstractservice;

import com.google.common.collect.Lists;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : ${PACKAGE_NAME}
 */
public abstract class AbstractServicePagingStringtID<T extends Serializable> {

    /**
     * The DAO for the object that have a String ID
     *
     * @return The DAO
     */
    protected abstract PagingAndSortingRepository<T, String> getDaoStringID();

    /**
     * Find a T object by String id
     *
     * @param id The id
     * @return The object
     */
    public T findOne(String id) {
        return getDaoStringID().findOne(id);
    }

    /**
     * Find all T object
     *
     * @return A list of T object
     */
    public List<T> findAll() {
        return Lists.newArrayList(getDaoStringID().findAll());
    }

    /**
     * Find all T object with pagination specified
     *
     * @param page The page number
     * @param size The size of list
     * @return The list of object paginated
     */
    public Page<T> findPaginated(int page, int size) {
        return getDaoStringID().findAll(new PageRequest(page, size));
    }

    /**
     * Insert a T object
     *
     * @param entity The T object
     * @return The T ojbect created
     */
    public T create(T entity) {
        return getDaoStringID().save(entity);
    }

    /**
     * Update the T object specified
     *
     * @param entity The T object to update
     * @return The T object updated
     */
    public T update(T entity) {
        return getDaoStringID().save(entity);
    }

    /**
     * Delete the T object specified
     *
     * @param entity The T object
     */
    public void delete(T entity) {
        getDaoStringID().delete(entity);
    }

    /**
     * Delete the T object specified by its String id
     *
     * @param id The id of the T object
     */
    public void deleteById(String id) {
        getDaoStringID().delete(id);
    }
}
