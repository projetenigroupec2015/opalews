package fr.opale.persistence.service.abstractservice;

import com.google.common.collect.Lists;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : ${PACKAGE_NAME}
 */
public abstract class AbstractServicePagingIntID<T extends Serializable> {

    /**
     * @return The DAO for the object that have an integer ID
     */
    abstract PagingAndSortingRepository<T, Integer> getDaoIntID();

    /**
     * Find a T object by int id
     *
     * @param id The id
     * @return The object
     */
    public T findOne(int id) {
        return getDaoIntID().findOne(id);
    }

    /**
     * Find all T object
     *
     * @return A list of T object
     */
    public List<T> findAll() {
        return Lists.newArrayList(getDaoIntID().findAll());
    }

    /**
     * Find all T object with pagination specified
     *
     * @param page The page number
     * @param size The size of list
     * @return The list of object paginated
     */
    public Page<T> findPaginated(int page, int size) {
        return getDaoIntID().findAll(new PageRequest(page, size));
    }

    /**
     * Insert a T object
     *
     * @param entity The T object
     * @return The T ojbect created
     */
    public T create(T entity) {
        return getDaoIntID().save(entity);
    }

    /**
     * Update the T object specified
     *
     * @param entity The T object to update
     * @return The T object updated
     */
    public T update(T entity) {
        return getDaoIntID().save(entity);
    }

    /**
     * Delete the T object specified
     *
     * @param entity The T object
     */
    public void delete(T entity) {
        getDaoIntID().delete(entity);
    }

    /**
     * Delete the T object specified by its int id
     *
     * @param id The id of the T object
     */
    public void deleteById(int id) {
        getDaoIntID().delete(id);
    }
}
