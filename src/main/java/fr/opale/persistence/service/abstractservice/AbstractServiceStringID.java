package fr.opale.persistence.service.abstractservice;

import com.google.common.collect.Lists;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 08/06/16
 * Package : fr.opale.persistence.service.abstractservice
 */
public abstract class AbstractServiceStringID<T extends Serializable> {

    /**
     * @return The DAO for the object that have an integer ID
     */
    protected abstract CrudRepository<T, String> getDaoStringID();

    /**
     * Find a T object by int id
     *
     * @param id The id
     * @return The object
     */
    public T findOne(String id) {
        return getDaoStringID().findOne(id);
    }

    /**
     * Find all T object
     *
     * @return A list of T object
     */
    public List<T> findAll() {
        return Lists.newArrayList(getDaoStringID().findAll());
    }

    /**
     * Insert a T object
     *
     * @param entity The T object
     * @return The T ojbect created
     */
    public T create(T entity) {
        return getDaoStringID().save(entity);
    }

    /**
     * Update the T object specified
     *
     * @param entity The T object to update
     * @return The T object updated
     */
    public T update(T entity) {
        return getDaoStringID().save(entity);
    }

    /**
     * Delete the T object specified
     *
     * @param entity The T object
     */
    public void delete(T entity) {
        getDaoStringID().delete(entity);
    }

    /**
     * Delete the T object specified by its int id
     *
     * @param id The id of the T object
     */
    public void deleteById(String id) {
        getDaoStringID().delete(id);
    }

    /**
     * Get the total number of the T object
     * @return The total
     */
    public Integer getTotal() {
        return Math.toIntExact(getDaoStringID().count());
    }

    /**
     * Check if the entity exist
     * @param id The entity ID
     * @return true id exist ; false if not
     */
    public boolean isExisting(String id){
        return getDaoStringID().exists(id);
    }

}

