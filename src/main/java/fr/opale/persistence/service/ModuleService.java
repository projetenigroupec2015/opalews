package fr.opale.persistence.service;

import fr.opale.persistence.dao.ModuleDao;
import fr.opale.persistence.model.Competence;
import fr.opale.persistence.model.Equipement;
import fr.opale.persistence.model.Module;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 13/08/16
 * Package : fr.opale.persistence.service
 */
@Service
public class ModuleService extends AbstractServiceIntID<Module>{

    @Autowired
    private ModuleDao dao;

    /**
     * @return The DAO for the object that have an integer ID
     */
    @Override
    protected CrudRepository<Module, Integer> getDaoIntID() {
        return dao;
    }

    public List<Competence> findCompetenceByModule(int idModule){
        return dao.findCompetenceByModule(idModule);
    }

    public List<Equipement> findEquipementsByModule(int idModule){
        return dao.findEquipementsByModule(idModule);
    }
}
