package fr.opale.persistence.service;

import fr.opale.persistence.dao.SalleDao;
import fr.opale.persistence.model.Lieu;
import fr.opale.persistence.model.Salle;
import fr.opale.persistence.service.abstractservice.AbstractServiceStringID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 13/07/16
 * Package : fr.opale.persistence.service
 */
@Service
public class SalleService extends AbstractServiceStringID<Salle> {

    @Autowired
    private SalleDao dao;

    /**
     * @return The DAO for the object that have an integer ID
     */
    @Override
    protected CrudRepository<Salle, String> getDaoStringID() {
        return dao;
    }

    public List<Salle> findAllSalles(){
        return dao.findAllSalles();
    }

    public List<Salle> findAllSallesByLieu(Lieu lieu) {
        return dao.findAllSallesByLieu(lieu);
    }

    @Transactional
    public int updateDisponibilite(Salle salle) {
        return dao.updateDisponibilite(salle.getCode(), salle.isDisponibilite());
    }
}
