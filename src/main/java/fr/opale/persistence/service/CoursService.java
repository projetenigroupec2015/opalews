package fr.opale.persistence.service;

import fr.opale.persistence.dao.CoursDao;
import fr.opale.persistence.model.Cours;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by JEREMY on 15/07/2016.
 */
@Service
public class CoursService extends AbstractServiceIntID<Cours> {

    @Autowired
    private CoursDao dao;

    public List<Cours> findAllCours(LocalDateTime dateStart, LocalDateTime dateEnd){
        return dao.findAllCours(dateStart, dateEnd);
    }

    @Override
    protected CrudRepository<Cours, Integer> getDaoIntID() {
        return dao;
    }

    @Transactional
    public void updateCours(Cours cours){
        dao.updateCours(cours.getId(), cours.getEcf(),cours.getSalle(),cours.getFormateur());
    }
}
