package fr.opale.persistence.service;

import fr.opale.persistence.dao.EcfDao;
import fr.opale.persistence.model.Ecf;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Brice BRUNEAU
 * Date : 09/06/2016
 * Package : fr.opale.persistence.service
 */
@Service
public class EcfService extends AbstractServiceIntID<Ecf> {

    @Autowired
    private EcfDao dao;

    @Override
    protected CrudRepository<Ecf, Integer> getDaoIntID() {
        return dao;
    }
}
