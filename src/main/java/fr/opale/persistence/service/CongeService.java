package fr.opale.persistence.service;

import fr.opale.enumeration.EtatCongeEnum;
import fr.opale.persistence.dao.CongeDao;
import fr.opale.persistence.model.Conge;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import fr.opale.web.exception.InvalideMethodParameterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 09/06/2016
 * Package : fr.opale.persistence.service
 */
@Service
public class CongeService extends AbstractServiceIntID<Conge> {

    @Autowired
    private CongeDao dao;

    @Override
    protected PagingAndSortingRepository<Conge, Integer> getDaoIntID() {
        return dao;
    }

    public Collection<Conge> findActiveByFormateurInPeriod(
            Formateur formateur,
            LocalDateTime dateStart,
            LocalDateTime dateEnd
    ) {
        return dao.findActiveByFormateurInPeriod(formateur, dateStart, dateEnd);
    }

    public Collection<Conge> findByFormateurInPeriod(
            Formateur formateur,
            LocalDateTime dateStart,
            LocalDateTime dateEnd
    ) {
        return dao.findByFormateurInPeriod(formateur, dateStart, dateEnd);
    }

    public Collection<Conge> findActiveInPeriod(
            LocalDateTime dateStart,
            LocalDateTime dateEnd
    ) {
        return dao.findActiveInPeriod(dateStart, dateEnd);
    }

    public Collection<Conge> findByEtatCongeInPeriod(
            EtatCongeEnum etatConge,
            LocalDateTime dateStart,
            LocalDateTime dateEnd
    ) {
        return dao.findByEtatCongeInPeriod(etatConge, dateStart, dateEnd);
    }

    public Collection<Conge> findInPeriod(
            LocalDateTime dateStart,
            LocalDateTime dateEnd
    ) {
        return dao.findInPeriod(dateStart, dateEnd);
    }

    public Collection<Conge> findByFormateurAndEtatCongeInPeriod(
            Formateur formateur,
            EtatCongeEnum etatConge,
            LocalDateTime dateStart,
            LocalDateTime dateEnd
    ) {
        return dao.findByFormateurAndEtatCongeInPeriod(formateur, etatConge, dateStart, dateEnd);
    }

    public boolean isValidConge(Conge conge) {
        if (conge == null) {
            throw new InvalideMethodParameterException();
        }

        Collection<Conge> conges = findActiveByFormateurInPeriod(conge.getFormateur(), conge.getDateDebutConge(), conge.getDateFinConge());
        if (conges == null || conges.size() == 0) {
            return true;
        } else if (conges.size() == 1) {
            Conge firstCongeCollection = conges.iterator().next();
            return firstCongeCollection.getIdConge() == conge.getIdConge();
        } else {
            return false;
        }
    }
}
