package fr.opale.persistence.service;

import fr.opale.persistence.dao.AffectationLieuDao;
import fr.opale.persistence.model.AffectationLieu;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * OpaleWS - fr.opale.persistence.service
 * <p>
 * Created by Thomas Croguennec on 22/10/16.
 * On 22/10/16
 */
@Service
public class AffectationLieuService extends AbstractServiceIntID{

    /**
     * @return The DAO for the object that have an integer ID
     */
    @Autowired
    AffectationLieuDao dao;

    @Override
    protected CrudRepository getDaoIntID() {
        return dao;
    }

    public List<AffectationLieu> findAllAffectationByIdFormateur(Formateur formateur){
        return dao.findAllAffectationByIdFormateur(formateur);
    };

}
