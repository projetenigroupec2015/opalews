package fr.opale.persistence.service;

import fr.opale.persistence.dao.FormationDao;
import fr.opale.persistence.model.Formation;
import fr.opale.persistence.service.abstractservice.AbstractServiceStringID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * Created by JEREMY on 26/07/2016.
 */
@Service
public class FormationService extends AbstractServiceStringID<Formation> {

    @Autowired
    private FormationDao dao;

    @Override
    protected CrudRepository<Formation, String> getDaoStringID() {
        return dao;
    }
}
