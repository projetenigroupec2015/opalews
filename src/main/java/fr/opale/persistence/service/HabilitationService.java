package fr.opale.persistence.service;

import fr.opale.persistence.dao.HabilitationDao;
import fr.opale.persistence.model.Habilitation;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Thomas Croguennec
 * Date : 26/09/16
 * Package : fr.opale.persistence.service
 */
@Service
public class HabilitationService extends AbstractServiceIntID<Habilitation>{

    @Autowired
    private HabilitationDao dao;

    /**
     * @return The DAO for the object that have an integer ID
     */
    @Override
    protected CrudRepository<Habilitation, Integer> getDaoIntID() {
        return dao;
    }


}
