package fr.opale.persistence.service;

import fr.opale.persistence.dao.PisteAuditDao;
import fr.opale.persistence.model.PisteAudit;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * OpaleWS - fr.opale.persistence.service
 * <p>
 * Created by Thomas Croguennec on 22/10/16.
 * On 22/10/16
 */
@Service
public class PisteAuditService extends AbstractServiceIntID<PisteAudit> {

    @Autowired
    PisteAuditDao dao;

    @Override
    protected CrudRepository<PisteAudit, Integer> getDaoIntID() {
        return dao;
    }
}
