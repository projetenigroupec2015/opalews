package fr.opale.persistence.service;

import fr.opale.enumeration.NotificationEnum;
import fr.opale.persistence.dao.NotificationDao;
import fr.opale.persistence.model.Conge;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.model.Notification;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import fr.opale.web.exception.IncompleteObjectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 23/10/2016
 * Package : fr.opale.persistence.service
 */
@Service
public class NotificationService extends AbstractServiceIntID<Notification> {

    @Autowired
    private NotificationDao dao;

    @Override
    protected CrudRepository<Notification, Integer> getDaoIntID() {
        return dao;
    }

    public Collection<Notification> getAllNotification() {
        return dao.getAllNotification();
    };

    public Notification createNotification(Conge conge) {
        Notification notif = new Notification();
        notif.setCodeAlerte(NotificationEnum.CONGE);
        notif.setFormateur(conge.getFormateur());
        notif.setMessage(getMessageNotification(conge));

        return create(notif);
    }

    public void supprimerNotification(Conge conge) {
        if (conge != null && conge.getFormateur() != null) {
            Notification notif = getNotificationByFormateurAndMessage(
                    conge.getFormateur(),
                    getMessageNotification(conge)
            );
            if (notif != null) {
                delete(notif);
            }
        }
    }

    public Collection<Notification> getNotificationByFormateur(Formateur formateur) {
        return dao.getNotificationByFormateur(formateur);
    }

    private String getMessageNotification(Conge conge) {
        if (conge == null
            || conge.getFormateur() == null
            || conge.getDateDebutConge() == null
            || conge.getDateFinConge() == null
        ) {
            throw new IncompleteObjectException();
        }

        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("Une demande de congé de ");
        messageBuilder.append(conge.getFormateur().getPrenom());
        messageBuilder.append(" ");
        messageBuilder.append(conge.getFormateur().getNom());
        messageBuilder.append(" (du ");
        messageBuilder.append(conge.getDateDebutConge().toString().replace("T", " "));
        messageBuilder.append(" au ");
        messageBuilder.append(conge.getDateFinConge().toString().replace("T", " "));
        messageBuilder.append(") est en attente de validation.");

        return messageBuilder.toString();
    }

    private Notification getNotificationByFormateurAndMessage(Formateur formateur, String message) {
        return dao.getNotificationByFormateurAndMessage(formateur, message);
    }
}
