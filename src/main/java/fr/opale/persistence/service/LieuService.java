package fr.opale.persistence.service;

import fr.opale.persistence.dao.LieuDao;
import fr.opale.persistence.model.Lieu;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Thomas Croguennec
 * Date : 13/07/16
 * Package : fr.opale.persistence.service
 */
@Service
public class LieuService extends AbstractServiceIntID<Lieu> {

    @Autowired
    private LieuDao dao;
    /**
     * @return The DAO for the object that have an integer ID
     */
    @Override
    protected CrudRepository<Lieu, Integer> getDaoIntID() {
        return dao;
    }

    public Lieu findOneByName(String lieuName) {
        return dao.findOneByName(lieuName);
    }
}
