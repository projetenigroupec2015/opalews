package fr.opale.persistence.service;

import fr.opale.persistence.dao.CompetenceDao;
import fr.opale.persistence.model.Competence;
import fr.opale.persistence.service.abstractservice.AbstractServiceStringID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * OpaleWS - fr.opale.persistence.service
 * <p>
 * Created by kro on 21/10/16.
 * On 21/10/16
 */
@Service
public class CompetenceService extends AbstractServiceStringID<Competence> {

    @Autowired
    private CompetenceDao dao;

    @Override
    protected CrudRepository<Competence, String> getDaoStringID() {
        return dao;
    }
}
