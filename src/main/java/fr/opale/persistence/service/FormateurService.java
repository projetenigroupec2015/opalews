package fr.opale.persistence.service;

import fr.opale.persistence.dao.FormateurDao;
import fr.opale.persistence.model.AffectationLieu;
import fr.opale.persistence.model.Cours;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Brice BRUNEAU
 * Date : 24/07/2016
 * Package : fr.opale.persistence.service
 */
@Service
public class FormateurService extends AbstractServiceIntID<Formateur> {

    @Autowired
    private FormateurDao dao;

    public List<Formateur> findAllFormateur(){
        return dao.findAllFormateur();
    }

    @Override
    protected PagingAndSortingRepository<Formateur, Integer> getDaoIntID() {
        return dao;
    }

    public Formateur findByUsername(String login){
        return dao.findByUsername(login);
    }

    public int findExperiencesByFormateurAndModule(Integer idFormateur, Integer idModule){
        return dao.findCountCoursByFormateurAndModule(idFormateur, idModule);
    }

    public List<Cours> findAllCoursByFormateur(Integer idFormateur, LocalDateTime dateStart, LocalDateTime dateEnd){
        return dao.findAllCoursByFormateur(idFormateur,dateStart,dateEnd);
    }

    public List<AffectationLieu> findDeplacementsExterieurFormateur(Integer idFormateur){
        return dao.findDeplacementsExterieurFormateur(idFormateur);
    }

    public Formateur findByUsernameWithHabilitation(String login){
        return dao.findByUsernameWithHabilitation(login);
    }

    public List<String> findAllUsername(){
        return dao.findAllUsername();
    }

    @Transactional
    public int updateHabilitation(Formateur formateur){
        return dao.updateHabilitation(formateur.getHabilitation(), formateur.getIdFormateur());
    }
}
