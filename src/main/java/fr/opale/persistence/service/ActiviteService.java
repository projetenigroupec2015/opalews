package fr.opale.persistence.service;

import fr.opale.persistence.dao.ActiviteDao;
import fr.opale.persistence.model.Activite;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Brice BRUNEAU
 * Date : 09/06/2016
 * Package : fr.opale.persistence.service
 */
@Service
public class ActiviteService extends AbstractServiceIntID<Activite> {

    @Autowired
    private ActiviteDao dao;

    @Override
    protected PagingAndSortingRepository<Activite, Integer> getDaoIntID() {
        return dao;
    }
}
