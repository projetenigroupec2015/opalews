package fr.opale.persistence.service;

import fr.opale.persistence.dao.FicheTempsDao;
import fr.opale.persistence.model.FicheTemps;
import fr.opale.persistence.model.Formateur;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 09/06/2016
 * Package : fr.opale.persistence.service
 */
@Service
public class FicheTempsService extends AbstractServiceIntID<FicheTemps> {
    @Autowired
    private FicheTempsDao dao;

    @Override
    protected PagingAndSortingRepository<FicheTemps, Integer> getDaoIntID() {
        return dao;
    }


    public Collection<FicheTemps> findByArchivedAndDate(
            boolean archived,
            Integer month,
            Integer year
    ) {
        return dao.findByArchivedAndDate(archived, month, year);
    }

    public Collection<FicheTemps> findByArchivedAndValidAndDate(
            boolean archived,
            boolean valid,
            Integer month,
            Integer year
    ) {
        return dao.findByArchivedAndValidAndDate(archived, valid, month, year);
    }

    public Collection<FicheTemps> findInPeriod(
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findInPeriod(monthStart, yearStart, monthEnd, yearEnd);
    }

    public Collection<FicheTemps> findByArchivedInPeriod(
            boolean archived,
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findByArchivedInPeriod(archived, monthStart, yearStart, monthEnd, yearEnd);
    }

    public Collection<FicheTemps> findByFormateurInPeriod(
            Formateur formateur,
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findByFormateurInPeriod(formateur,monthStart,yearStart,monthEnd,yearEnd);
    }

    public Collection<FicheTemps> findByFormateurAndArchivedInPeriod(
            Formateur formateur,
            boolean archived,
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findByFormateurAndArchivedInPeriod(formateur, archived, monthStart, yearStart, monthEnd,yearEnd);
    }

    public Collection<FicheTemps> findByDate(
            Integer month,
            Integer year
    ) {
        return dao.findByDate(month, year);
    }

    public FicheTemps findByFormateurAndDate(
            Formateur formateur,
            Integer month,
            Integer year
    ) {
        return dao.findByFormateurAndDate(formateur, month,year);
    }

    public Collection<FicheTemps> findByValidInPeriod(
            boolean valid,
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findByValidInPeriod(valid, monthStart, yearStart, monthEnd, yearEnd);
    }

    public Collection<FicheTemps> findByArchivedAndValidInPeriod(
            boolean archived,
            boolean valid,
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findByArchivedAndValidInPeriod(archived, valid, monthStart, yearStart, monthEnd, yearEnd);
    }

    public Collection<FicheTemps> findByFormateurAndValidInPeriod(
            Formateur formateur,
            boolean valid,
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findByFormateurAndValidInPeriod(formateur, valid, monthStart, yearStart, monthEnd, yearEnd);
    }

    public Collection<FicheTemps> findByFormateurAndArchivedAndValidInPeriod(
            Formateur formateur,
            boolean archived,
            boolean valid,
            Integer monthStart,
            Integer yearStart,
            Integer monthEnd,
            Integer yearEnd
    ) {
        return dao.findByFormateurAndArchivedAndValidInPeriod(formateur, archived, valid, monthStart, yearStart, monthEnd, yearEnd);
    }

    public Collection<FicheTemps> findByValidAndDate(
            boolean valid,
            Integer month,
            Integer year
    ) {
        return dao.findByValidAndDate(valid, month, year);
    }
}
