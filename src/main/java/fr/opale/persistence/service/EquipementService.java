package fr.opale.persistence.service;

import fr.opale.persistence.dao.EquipementDao;
import fr.opale.persistence.model.Equipement;
import fr.opale.persistence.model.Lieu;
import fr.opale.persistence.model.Salle;
import fr.opale.persistence.service.abstractservice.AbstractServiceIntID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 16/07/16
 * Package : fr.opale.persistence.service
 */
@Service
public class EquipementService extends AbstractServiceIntID<Equipement> {

    @Autowired
    private EquipementDao dao;
    /**
     * @return The DAO for the object that have an integer ID
     */
    @Override
    protected CrudRepository<Equipement, Integer> getDaoIntID() {
        return dao;
    }
}
