package fr.opale.persistence.service;

import fr.opale.persistence.dao.ParametrageDao;
import fr.opale.persistence.model.Parametrage;
import fr.opale.persistence.service.abstractservice.AbstractServiceStringID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : fr.opale.persistence.service
 */
@Service
public class ParametrageService extends AbstractServiceStringID<Parametrage> {

    @Autowired
    private ParametrageDao dao;

    /**
     * The DAO for the object that have a String ID
     *
     * @return The DAO
     */
    @Override
    protected CrudRepository<Parametrage, String> getDaoStringID() {
        return dao;
    }

    public List<Parametrage> findAllAlertes(){
        return dao.findAllAlertes();
    }

    public List<Parametrage> findAllParams(){
        return dao.findAllParams();
    }

    @Transactional
    public int updateConfiguration(String codeParam, String value){
        return dao.saveConfiguration(codeParam, value);
    }


}
