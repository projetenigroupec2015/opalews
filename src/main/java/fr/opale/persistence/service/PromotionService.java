package fr.opale.persistence.service;
import fr.opale.persistence.dao.PromotionDao;
import fr.opale.persistence.model.Promotion;
import fr.opale.persistence.service.abstractservice.AbstractServiceStringID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by JEREMY on 14/08/2016.
 */
@Service
public class PromotionService extends AbstractServiceStringID<Promotion> {

    @Autowired
    private PromotionDao dao;

    @Override
    protected CrudRepository<Promotion, String> getDaoStringID() {
        return dao;
    }

    public List<Promotion> findAllPromotions(LocalDateTime dateStart, LocalDateTime dateEnd){
        return dao.findAllPromotions(dateStart, dateEnd);
    }
}
