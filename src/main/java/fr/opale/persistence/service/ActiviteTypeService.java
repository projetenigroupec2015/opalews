package fr.opale.persistence.service;

import fr.opale.persistence.dao.ActiviteTypeDao;
import fr.opale.persistence.model.TypeActivite;
import fr.opale.persistence.service.abstractservice.AbstractServicePagingStringtID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Brice BRUNEAU
 * Date : 09/06/2016
 * Package : fr.opale.persistence.service
 */
@Service
public class ActiviteTypeService extends AbstractServicePagingStringtID<TypeActivite> {

    @Autowired
    private ActiviteTypeDao dao;

    @Override
    protected PagingAndSortingRepository<TypeActivite, String> getDaoStringID() {
        return dao;
    }
}
