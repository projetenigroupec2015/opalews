package fr.opale.persistence.attributeConverter;

import fr.opale.enumeration.FacturationEnum;
import fr.opale.enumeration.NotificationEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by JEREMY on 18/10/2016.
 */
@Converter
public class NotificationEnumConverter implements AttributeConverter<NotificationEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(NotificationEnum notificationEnum) {
        return (notificationEnum == null ? 0 : notificationEnum.getNumero());
    }

    @Override
    public NotificationEnum convertToEntityAttribute(Integer numero) {
        return NotificationEnum.getNotificationEnum(numero);
    }

}
