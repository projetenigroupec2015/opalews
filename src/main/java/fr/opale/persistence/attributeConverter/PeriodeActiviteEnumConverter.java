package fr.opale.persistence.attributeConverter;

import fr.opale.enumeration.PeriodeActiviteEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Brice BRUNEAU
 * Date : 31/07/2016
 * Package : fr.opale.persistence.attributeConverter
 */
@Converter
public class PeriodeActiviteEnumConverter implements AttributeConverter<PeriodeActiviteEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(PeriodeActiviteEnum periodeActiviteEnum) {
        return (periodeActiviteEnum == null ? 0 : periodeActiviteEnum.getNumber());
    }

    @Override
    public PeriodeActiviteEnum convertToEntityAttribute(Integer numero) {
        return PeriodeActiviteEnum.getEnum(numero);
    }
}
