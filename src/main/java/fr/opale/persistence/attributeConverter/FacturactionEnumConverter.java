package fr.opale.persistence.attributeConverter;

import fr.opale.enumeration.FacturationEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by JEREMY on 16/10/2016.
 */
@Converter
public class FacturactionEnumConverter implements AttributeConverter<FacturationEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(FacturationEnum facturationEnum) {
        return (facturationEnum == null ? 0 : facturationEnum.getNumero());
    }

    @Override
    public FacturationEnum convertToEntityAttribute(Integer numero) {
        return (numero == null ? FacturationEnum.getEtatCongeEnum(0) : FacturationEnum.getEtatCongeEnum(numero));
    }
}
