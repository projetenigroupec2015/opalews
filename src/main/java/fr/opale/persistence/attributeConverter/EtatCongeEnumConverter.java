package fr.opale.persistence.attributeConverter;

import fr.opale.enumeration.EtatCongeEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Brice on 07/06/2016.
 */
@Converter
public class EtatCongeEnumConverter implements AttributeConverter<EtatCongeEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EtatCongeEnum etatCongeEnum) {
        return (etatCongeEnum == null ? 0 : etatCongeEnum.getNumber());
    }

    @Override
    public EtatCongeEnum convertToEntityAttribute(Integer numero) {
        return EtatCongeEnum.getEnum(numero);
    }
}
