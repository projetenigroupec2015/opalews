package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.model
 */
@Entity
@Data
@NoArgsConstructor
@Table(name="FICHE_TEMPS",
        uniqueConstraints = @UniqueConstraint(name = "formateur_date_uniq", columnNames = {"ANNEEFICHETEMPS", "MOISFICHETEMPS", "FORMATEURID"}))
public class FicheTemps implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 3299953131369949136L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFICHETEMPS")
    private int idFicheTemps;

    @Column(name = "ISVALIDE", columnDefinition = "bit default 0", nullable = false)
    private boolean valide;

    @Column(name="ISARCHIVE", columnDefinition = "bit default 0", nullable = false)
    private boolean archive;

    @Column(name="MOISFICHETEMPS", nullable = false)
    private int moisFicheTemps;

    @Column(name="ANNEEFICHETEMPS", nullable = false)
    private int anneeFicheTemps;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FORMATEURID", nullable = false)
    private Formateur formateur;

    @OneToMany(mappedBy = "ficheTemps", cascade = CascadeType.ALL)
    @JsonManagedReference("activite-fichetemps")
    private Collection<Activite> activites;
}
