package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : ${PACKAGE_NAME}
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "CONFIGURATION")
public class Configuration implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 931779594102896306L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDCONFIG", nullable = false, unique = true, length = 30)
    private int id;

    @Column(name = "VALEUR", nullable = false, length = 300)
    private String valeur;

    @Column(name = "UNITE", length = 30)
    private String unite;

    @JsonBackReference("configuration-parametrage")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODEPARAM")
    private Parametrage parametre;
}
