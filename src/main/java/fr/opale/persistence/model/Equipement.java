package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Brice BRUNEAU
 * Date : 25/06/2016
 * Package : fr.opale.persistence.model
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "EQUIPEMENT")
public class Equipement implements Serializable{

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -3583151426101903403L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDEQUIPEMENT", nullable = false)
    private int id;

    @Column(name = "LOTEQUIPEMENT", length = 4)
    private String lot;

    @Column(name = "MODELEEQUIPEMENT", length = 50)
    private String modele;

    @Column(name = "RAMEQUIPEMENT", length = 3)
    private String ram;

    @Column(name = "CPUEQUIPEMENT", length = 15)
    private String cpu;

    @Column(name = "FINGARANTIE")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date finGarantie;

    @Column(name = "TYPEEQUIPEMENT", length = 30)
    private String type;

    @Column(name = "DISQUE", length = 50)
    private String disque;

    @Column(name = "DERNIERNETTOYAGE")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dernierNettoyage;

    @JsonBackReference("salle-equipement")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDSALLE", nullable = false)
    private Salle salle;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "MODULE_EQUIPEMENT",
            uniqueConstraints = @UniqueConstraint(name = "module_equip_uniq", columnNames = {"IDMODULE", "IDEQUIPEMENT"}),
            joinColumns = @JoinColumn(name = "IDEQUIPEMENT", referencedColumnName = "IDEQUIPEMENT"),
            inverseJoinColumns = @JoinColumn(name = "IDMODULE", referencedColumnName = "IDMODULE"))
    private Collection<Module> modules;
}
