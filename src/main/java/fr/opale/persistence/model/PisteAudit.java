package fr.opale.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by daphne on 16/06/2016.
 */
@Entity
@Data
@Table(name = "PISTE_AUDIT")
public class PisteAudit implements Serializable{

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -2955539083752050500L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDAUDIT")
    private  int id;

    @Column(name = "USERAUDIT", length = 50)
    private String user;

    @Column(name = "DATEAUDIT")
    private LocalDateTime date;

    @Column(name = "ACTIONAUDIT", length = 1500)
    private String action;

    /**
     * Default constructor
     */
    public PisteAudit() {
    }
}
