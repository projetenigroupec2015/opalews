package fr.opale.persistence.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Brice BRUNEAU
 * Date : 25/06/2016
 * Package : fr.opale.persistence.model
 */
@Embeddable
@NoArgsConstructor
public class ExperienceId implements Serializable{

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 4141742071645535140L;

    @Getter
    @Setter
    private Module module;
    @Getter
    @Setter
    private Formateur formateur;

}
