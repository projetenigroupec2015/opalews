package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import fr.opale.enumeration.FacturationEnum;
import fr.opale.persistence.attributeConverter.FacturactionEnumConverter;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Data
@DynamicUpdate
@NoArgsConstructor
@Table(name = "COURS")
public class Cours implements Serializable{

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 5928936140741261744L;

    @Id
    @Column(name = "IDCOURS")
    private String id;

    @Column(name = "DEBUTCOURS")
    private LocalDateTime debut;

    @Column(name = "FINCOURS")
    private LocalDateTime fin;

    @Column(name = "DUREEREELLEENHEURES")
    private int dureeReelleEnHeures;

    @Column(name = "DATECREATIONCOURS")
    private LocalDateTime dateCreation;

    @Column(name = "DATEMODIFCOURS")
    private LocalDateTime dateModif;

    @Column(name = "LIBELLECOURS", length = 50)
    private String libelle;

    @Column(name = "DUREEPREVUEENHEURES")
    private int dureePrevueEnHeures;

    @Column(name = "DATEADEFINIR", columnDefinition = "bit default 0", nullable = false)
    private boolean dateADefinir;

    /*@Column(name = "FACTURATIONCOURS", columnDefinition = "bit default 0", nullable = false)
    private boolean facturation;*/

    @Convert(converter = FacturactionEnumConverter.class)
    @Column(name = "FACTURATIONCOURS")
    private FacturationEnum facturation;

    @Column(name = "DEMANDECOURSEXTERNE"/*, nullable = false, columnDefinition = "INT DEFAULT 0"*/)
    private Integer demandeCoursExterne = 0;

    @Column(name = "NBSTAGIAIRE")
    private int nbStagiaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDMODULE", nullable = false)
    private Module module;

//    @OneToOne(mappedBy = "cours", fetch = FetchType.LAZY)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDECF")
    private Ecf ecf;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODEPROMOTION")
    private Promotion promotion;

    @JsonManagedReference("planningbrouillon-cours")
    @OneToMany(targetEntity = PlanningBrouillon.class, mappedBy = "cours", fetch = FetchType.LAZY)
    private Collection<PlanningBrouillon> planningBrouillon;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODESALLE")
    private Salle salle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDFORMATEUR")
    private Formateur formateur;
}
