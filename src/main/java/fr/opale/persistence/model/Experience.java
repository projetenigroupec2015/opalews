package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by daphne on 14/06/2016.
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "EXPERIENCE")
@IdClass(ExperienceId.class)
public class Experience implements Serializable{

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -8817948205210978763L;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDMODULE")
    private Module module;

    @JsonBackReference("experience-formateur")
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDFORMATEUR")
    private Formateur formateur;

    @Column(name = "ISDEBUTANT", columnDefinition = "bit default 0", nullable = false)
    private boolean isDebutant;

    @Column(name = "NBDISPENSE", nullable = false)
    private int nbDispense;
}
