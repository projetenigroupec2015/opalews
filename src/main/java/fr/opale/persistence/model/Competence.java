package fr.opale.persistence.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by daphne on 14/06/2016.
 */
@Entity
@Data
@Table(name = "COMPETENCE")
public class Competence implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -8022740735474647750L;

    @Id
    @Column(name = "CODECOMPETENCE", nullable = false, unique = true, length = 20)
    private String code;

    @Column(name = "LIBELLECOMPETENCE", nullable = false, length = 50)
    private String libelle;


    /**
     * Default constructor
     */
    public Competence() {
    }
}
