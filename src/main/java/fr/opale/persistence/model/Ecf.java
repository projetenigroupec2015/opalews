package fr.opale.persistence.model;

/**
 * Created by daphne on 09/06/2016.
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by daphne
 * Date: 09/06/2016.
 */
@Entity
@Data
@DynamicUpdate
@NoArgsConstructor
@Table(name = "ECF")
public class Ecf implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -5096947635730345338L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDECF")
    private int id;

    @Column(name = "LIBELLEECF", length = 200)
    private String libelle;

    @Column(name = "DATEECF")
    private LocalDateTime date;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATECREATION")
    private Date dateCreation;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATEMODIFICATIONECF")
    private Date dateModification;

    @Column(name = "ISCORRIGE", columnDefinition = "bit default 0", nullable = false)
    private boolean corrige;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCOURS", nullable = false, unique = true)
    private Cours cours;
}
