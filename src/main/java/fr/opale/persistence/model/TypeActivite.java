package fr.opale.persistence.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.model
 */
@Entity
@Data
@Table(name = "TYPE_ACTIVITE")
public class TypeActivite implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 2360572563245164788L;

    @Id
    @Column(name = "CODEACTIVITE", length = 8, unique = true)
    private String codeActivite;

    @Column(name = "LIBELLEACTIVITE", length = 200)
    private String libelleActivite;

    /**
     * Default constructor
     */
    public TypeActivite() {
    }

}
