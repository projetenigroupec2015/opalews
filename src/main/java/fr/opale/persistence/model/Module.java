package fr.opale.persistence.model;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@DynamicUpdate
@Data
@Table(name = "MODULE")
public class Module implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -5954271579352339870L;

    @Id
    @Column(name = "IDMODULE")
    private int id;

    @Column(name = "LIBELLEMODULE", length = 200)
    private String libelle;

    @Column(name = "DATECREATIONMODULE")
    private LocalDateTime dateCreation;

    @Column(name = "DUREEENHEURES")
    private int dureeEnHeures;

    @Column(name = "DUREEENSEMAINES")
    private int dureeEnSemaines;

    @Column(name = "LIBELLECOURT", length = 20)
    private String libelleCourt;

    @Column(name = "DATEMODIFMODULE")
    private LocalDateTime dateModif;

    @Column(name = "ARCHIVER", columnDefinition = "bit default 0", nullable = false)
    private boolean archiver;

    @Column(name = "DETAIL")
    private String detail;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "MODULE_COMPETENCE",
            uniqueConstraints = @UniqueConstraint(name = "module_comp_uniq", columnNames = {"IDMODULE", "CODECOMPETENCE"}),
            joinColumns = @JoinColumn(name = "IDMODULE", referencedColumnName = "IDMODULE"),
            inverseJoinColumns = @JoinColumn(name = "CODECOMPETENCE", referencedColumnName = "CODECOMPETENCE"))
    private Collection<Competence> competences;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "MODULE_EQUIPEMENT",
            uniqueConstraints = @UniqueConstraint(name = "module_equip_uniq", columnNames = {"IDMODULE", "IDEQUIPEMENT"}),
            joinColumns = @JoinColumn(name = "IDMODULE", referencedColumnName = "IDMODULE"),
            inverseJoinColumns = @JoinColumn(name = "IDEQUIPEMENT", referencedColumnName = "IDEQUIPEMENT"))
    private Collection<Equipement> equipements;

    @OneToMany(mappedBy= "module", fetch= FetchType.LAZY)
    private Collection<Cours> cours;

    /**
     * Default constructor
     */
    public Module() {
    }

}
