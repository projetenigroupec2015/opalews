package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by daphne
 * Date: 09/06/2016.
 */
@Entity
@DynamicUpdate
@Data
@Table(name = "SALLE")
public class Salle implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 633235378015835415L;

    @Id
    @Column(name = "CODESALLE", nullable = false, unique = true, length = 5)
    private String code;

    @Column(name = "LIBELLESALLE", length = 50)
    private String libelle;

    @Column(name = "CAPACITESALLE")
    private int capacite;

    @Column(name = "DISPONIBILITESALLE", columnDefinition = "bit default 1", nullable = false)
    private boolean disponibilite;

//    @JsonBackReference("salle-lieu")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDLIEU", nullable = false, updatable = false, insertable = false)
    private Lieu lieu;

    @JsonManagedReference("salle-equipement")
    @OneToMany(targetEntity = Equipement.class, mappedBy = "salle", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Equipement> equipements;

}
