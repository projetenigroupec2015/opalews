package fr.opale.persistence.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Brice BRUNEAU
 * Date : 25/06/2016
 * Package : fr.opale.persistence.model
 */
@Data
@Embeddable
public class AffectationLieuId implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -3703253126765304960L;


    @Getter
    @Setter
//    @Column(name = "IDLIEU", nullable=false)
    private Lieu lieu;


    @Getter
    @Setter
//    @Column(name = "IDFORMATEUR", nullable = false)
    private Formateur formateur;

}
