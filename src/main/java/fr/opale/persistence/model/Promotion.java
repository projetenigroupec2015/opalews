package fr.opale.persistence.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by daphne on 09/06/2016.
 */
@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@Table(name = "PROMOTION")
public class Promotion implements Serializable{

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -8773982553762485750L;

    @Id
    @Column(name = "CODEPROMOTION")
    private String code;

    @Column(name = "LIBELLEPROMOTION", length = 50)
    private String libelle;

    @Column(name = "DEBUTPROMOTION")
    private LocalDateTime debut;

    @Column(name = "FINPROMOTION")
    private LocalDateTime fin;

    @Column(name = "DATEMODIFPROMOTION")
    private LocalDateTime dateModif;

    @Column(name = "DATECREATIONPROMOTION")
    private LocalDateTime dateCreation;
/*
    @OneToMany(mappedBy = "promotion", fetch = FetchType.LAZY)
    private Collection<Cours> cours;
*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODEFORMATION", updatable = false)
    private Formation formation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDLIEU")
    private Lieu lieu;

}
