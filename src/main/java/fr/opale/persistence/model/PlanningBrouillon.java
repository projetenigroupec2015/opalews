package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by daphne on 09/06/2016.
 */
@Entity
@Data
@Table(name = "PLANNING_BROUILLON")
public class PlanningBrouillon implements Serializable{

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -3353793227513044247L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDPLANNING")
    private int idPlanning;

    @Column(name = "LIBELLEPLANNING", length = 200)
    private String libellePlanning;

    @Column(name = "DATECREATIONPLANNING")
    private LocalDateTime dateCreationPlanning;

    @Column(name = "DEBUTCOURSBROUILLON")
    private LocalDateTime debutCoursBrouillon;

    @Column(name = "FINCOURSBROUILLON")
    private LocalDateTime finCoursBrouillon;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDFORMATEUR", nullable = true)
    private Formateur formateur;

    @JsonBackReference("planningbrouillon-cours")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCOURS", nullable = true)
    private Cours cours;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODESALLE", nullable = true)
    private Salle salle;

    /**
     * Default constructor
     */
    public PlanningBrouillon() {
    }

}
