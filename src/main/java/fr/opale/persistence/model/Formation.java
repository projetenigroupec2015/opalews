package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Created by daphne on 19/06/2016.
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "FORMATION")
public class Formation implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 8889319220391721860L;

    @Id
    @Column(name = "CODEFORMATION", nullable = false, unique = true, length = 8)
    private String code;

    @Column(name = "LIBELLELONG", length = 200)
    private String libelleLong;

    @Column(name = "LIBELLECOURSFORMATION", length = 50)
    private String libelleCourt;

    @Column(name = "DUREEENHEURESFORMATION")
    private int dureeEnHeures;

    @Column(name = "TAUXHORAIRE")
    private float tauxHoraire;

    @Column(name = "DATECREATIONFORMATION")
    private LocalDateTime dateCreation;

    @Column(name = "CODETITRE", length = 8, nullable = true)
    private String codeTitre;

    @Column(name = "HEURESCENTRE", nullable = true)
    private int heuresCentre;

    @Column(name = "HEURESSTAGE", nullable = true)
    private int heuresStage;

    @Column(name = "SEMAINESCENTRE", nullable = true)
    private int semainesCentre;

    @Column(name = "SEMAINESTAGE", nullable = true)
    private int semainesStage;

    @Column(name = "DUREEENSEMAINESFORMATION")
    private int dureeEnSemaines;

    @Column(name = "DATEMODIFFORMATION")
    private LocalDateTime dateModif;

    @Column(name = "ARCHIVERFORMATION", columnDefinition = "bit default 0", nullable = false)
    private boolean archiver;

    @Column(name = "ECFAPASSER", nullable = true)
    private int ecfAPasser;

    @Column(name = "TYPE"/*, nullable = false*/)
    private String type;

    @JsonIgnore
    @OneToMany(targetEntity = Promotion.class, mappedBy = "formation", fetch = FetchType.LAZY)
    private Collection<Promotion> promotions;
}
