package fr.opale.persistence.model;

import fr.opale.enumeration.EtatCongeEnum;
import fr.opale.persistence.attributeConverter.EtatCongeEnumConverter;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.model
 */
@Entity
@Data
@NoArgsConstructor
@Table(name="CONGE")
public class Conge implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -7722559310629207690L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDCONGE")
    private int idConge;

    @Column(name="DATEDEBUTCONGE", nullable = false)
    private LocalDateTime dateDebutConge;

    @Column(name="DATEFINCONGE", nullable = false)
    private LocalDateTime dateFinConge;

    @Column(name = "MOTIF", nullable = true)
    private String motif;

    @Convert(converter = EtatCongeEnumConverter.class)
    @Column(name = "ETATCONGE", nullable = false)
    private EtatCongeEnum etatConge;

    @Column(name = "DATEDEMANDECONGE", nullable = false)
    private LocalDateTime dateDemandeConge;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FORMATEURID", nullable = false)
    private Formateur formateur;
}
