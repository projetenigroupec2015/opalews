package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : fr.opale.persistence.model
 */
@Entity
//@DynamicUpdate
@Data
@Table(name = "FORMATEUR")
public class Formateur implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -4612538163803587281L;

    @Id
    @Column(name = "IDFORMATEUR")
    private int idFormateur;

    //    @JsonBackReference("formateur-habilitation")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDHABILITATION")
    private Habilitation habilitation;

    @Column(name = "NOM", length = 100)
    private String nom;

    @Column(name = "PRENOM",length = 100)
    private String prenom;

    @Column(name = "ISEXTERNE", columnDefinition = "bit default 0", nullable = false)
    private boolean externe;

    @Column(name = "USERNAME", length = 50)
    private String username;

    @Column(name = "MAIL", length = 200)
    private String mail;

//    @JsonManagedReference("site-formateur")
    @JsonIgnore
    @OneToMany(mappedBy = "formateur", fetch = FetchType.LAZY)
    private Collection<AffectationLieu> affectationsLieux;

    @JsonManagedReference("experience-formateur")
    @OneToMany(mappedBy = "formateur", fetch = FetchType.LAZY)
    private Collection<Experience> experiences;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "FORMATEUR_COMPETENCE",
            uniqueConstraints = @UniqueConstraint(name = "form_comp_uniq", columnNames = {"IDFORMATEUR", "CODECOMPETENCE"}),
            joinColumns = @JoinColumn(name = "IDFORMATEUR", referencedColumnName = "IDFORMATEUR"),
            inverseJoinColumns = @JoinColumn(name = "CODECOMPETENCE", referencedColumnName = "CODECOMPETENCE"))
    private Collection<Competence> competences;

    @JsonIgnore
    @OneToMany(mappedBy = "formateur", fetch = FetchType.LAZY)
    private Collection<Conge> conges;

//    @Override
//    public String toString() {
//        return this.getPrenom().concat(" ").concat(this.getNom());
//    }
}
