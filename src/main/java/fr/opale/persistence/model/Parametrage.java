package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : fr.opale.persistence.model
 */
@Entity
@Data
@Table(name = "PARAMETRAGE")
public class Parametrage implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -8559676240449820102L;

    @Id
    @Column(name = "CODEPARAM", nullable = false, unique = true, length = 30)
    private String code;

    @Column(name = "LIBELLEPARAM", nullable = false, length = 200)
    private String libelle;

    @Column(name = "TYPEPARAM", nullable = false, length = 15)
    private String type;

    @Column(name = "TEXTEPARAM", nullable = false, length = 500)
    private String texteParam;

    @JsonManagedReference("configuration-parametrage")
    @OneToMany(targetEntity = Configuration.class, mappedBy = "parametre", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Configuration> configs;

}
