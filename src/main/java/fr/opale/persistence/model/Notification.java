package fr.opale.persistence.model;

import fr.opale.enumeration.NotificationEnum;
import fr.opale.persistence.attributeConverter.NotificationEnumConverter;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "NOTIFICATION")
public class Notification implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDNOTIFICATION")
    private int id;

    @Convert(converter = NotificationEnumConverter.class)
    @Column(name = "CODEALERTE")
    private NotificationEnum codeAlerte;

    @JoinColumn(name = "IDFORMATEUR")
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODULE")
    private Module module;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDFORMATEUR")
    private Formateur formateur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPARAMETRE")
    private Parametrage parametre;
}

