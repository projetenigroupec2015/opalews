package fr.opale.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by daphne on 21/06/2016.
 */
@Entity
//@DynamicUpdate
@Data
@Table(name = "AFFECTATION_LIEU",
        uniqueConstraints = @UniqueConstraint(name = "preferencesite_formateur_uniq", columnNames = {"PREFERENCELIEU", "IDFORMATEUR"}))
public class AffectationLieu implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 3252873894020414919L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDAFFECTATION")
    private int id;

//    @JsonBackReference("site-affectationlieux")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDLIEU")
    private Lieu lieu;

//    @JsonBackReference("site-formateur")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDFORMATEUR")
    private Formateur formateur;

    @Column(name = "ISDEFAULT", columnDefinition = "bit default 0", nullable = false)
    private boolean isDefault;

    @Column(name = "PREFERENCELIEU", nullable = false)
    private Integer preferenceLieu;

    /**
     * Default constructor
     */
    public AffectationLieu() {
    }
}
