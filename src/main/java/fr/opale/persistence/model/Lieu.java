package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by daphne on 09/06/2016.
 */
@Entity
@DynamicUpdate
@Data
@Table(name = "LIEU")
public class Lieu implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -206702517692382141L;

    @Id
    @Column(name = "IDLIEU")
    private int id;

    @Column(name = "LIBELLELIEU", length = 50)
    private String libelle;

//    @JsonManagedReference("salle-lieu")
    @JsonIgnore
    @OneToMany(targetEntity = Salle.class, mappedBy = "lieu", fetch = FetchType.EAGER)
    private Collection<Salle> salles;

//    @JsonManagedReference("site-affectationlieux")
    @JsonIgnore
    @OneToMany(mappedBy = "lieu", fetch = FetchType.EAGER)
    private Collection<AffectationLieu> affectationLieux;

}
