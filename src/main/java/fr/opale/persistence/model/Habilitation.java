package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Thomas Croguennec
 * Date : 07/06/16
 * Package : fr.opale.persistence.model
 */
@Entity
@Data
@Table(name = "HABILITATION")
public class Habilitation implements Serializable {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = -2676016340250906931L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDHABILITATION")
    private int id;

    @Column(name = "LIBELLEHABILITATION", length = 50)
    private String libelle;

//    @JsonManagedReference("formateur-habilitation")
    @JsonIgnore
    @OneToMany(targetEntity = Formateur.class, mappedBy = "habilitation", fetch = FetchType.LAZY)
    private Collection<Formateur> formateurs;

    /**
     * Default constructor
     */
    public Habilitation() {
    }

}
