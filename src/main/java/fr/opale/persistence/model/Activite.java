package fr.opale.persistence.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import fr.opale.enumeration.PeriodeActiviteEnum;
import fr.opale.persistence.attributeConverter.PeriodeActiviteEnumConverter;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by Brice BRUNEAU
 * Date : 07/06/2016
 * Package : fr.opale.persistence.model
 */
@Entity
@Data
@Table(name = "ACTIVITE",
        uniqueConstraints = @UniqueConstraint(name = "fichetemps_date_periode_uniq", columnNames = {"FICHETEMPSID", "DATEACTIVITE", "PERIODE"}))
public class Activite implements Serializable {
    /**
     * Serial ID
     */
    private static final long serialVersionUID = 1088746904436613688L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDACTIVITE")
    private int idActivite;

    @Column(name = "DATEACTIVITE", nullable = false)
    private LocalDate dateActivite;

    @Convert(converter = PeriodeActiviteEnumConverter.class)
    @Column(name = "PERIODE", nullable = false)
    private PeriodeActiviteEnum periodeActivite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODEACTIVITE", nullable = false)
    private TypeActivite typeActivite;

    @JsonBackReference("activite-fichetemps")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FICHETEMPSID", nullable = false)
    private FicheTemps ficheTemps;

    /**
     * Default constructor
     */
    public Activite() {
    }

    /**
     * Constructor with all params
     * @param dateActivite The date
     * @param periodeActivite the activity period
     * @param typeActivite the activity type
     * @param ficheTemps the timesheet
     */
    public Activite(LocalDate dateActivite, PeriodeActiviteEnum periodeActivite, TypeActivite typeActivite, FicheTemps ficheTemps) {
        this.dateActivite = dateActivite;
        this.periodeActivite = periodeActivite;
        this.typeActivite = typeActivite;
        this.ficheTemps = ficheTemps;
    }

    public int getIdActivite() {
        return idActivite;
    }

    public void setIdActivite(int idActivite) {
        this.idActivite = idActivite;
    }

    public LocalDate getDateActivite() {
        return dateActivite;
    }

    public void setDateActivite(LocalDate dateActivite) {
        this.dateActivite = dateActivite;
    }

    public PeriodeActiviteEnum getPeriodeActivite() {
        return periodeActivite;
    }

    public void setPeriodeActivite(PeriodeActiviteEnum periodeActivite) {
        this.periodeActivite = periodeActivite;
    }

    public TypeActivite getTypeActivite() {
        return typeActivite;
    }

    public void setTypeActivite(TypeActivite typeActivite) {
        this.typeActivite = typeActivite;
    }

    public FicheTemps getFicheTemps() {
        return ficheTemps;
    }

    public void setFicheTemps(FicheTemps ficheTemps) {
        this.ficheTemps = ficheTemps;
    }
}
