
    create table ACTIVITE (
        IDACTIVITE int identity not null,
        DATEACTIVITE varbinary(255) not null,
        PERIODE int not null,
        FICHETEMPSID int not null,
        CODEACTIVITE varchar(8) not null,
        primary key (IDACTIVITE)
    );

    create table AFFECTATION_LIEU (
        IDAFFECTATION int identity not null,
        ISDEFAULT bit default 0 not null,
        PREFERENCELIEU int not null,
        IDFORMATEUR int,
        IDLIEU int,
        primary key (IDAFFECTATION)
    );

    create table COMPETENCE (
        CODECOMPETENCE varchar(20) not null,
        LIBELLECOMPETENCE varchar(50) not null,
        primary key (CODECOMPETENCE)
    );

    create table CONFIGURATION (
        IDCONFIG int identity not null,
        UNITE varchar(30),
        VALEUR varchar(300) not null,
        CODEPARAM varchar(30),
        primary key (IDCONFIG)
    );

    create table CONGE (
        IDCONGE int identity not null,
        DATEDEBUTCONGE varbinary(255) not null,
        DATEDEMANDECONGE varbinary(255) not null,
        DATEFINCONGE varbinary(255) not null,
        ETATCONGE int not null,
        MOTIF varchar(255),
        FORMATEURID int not null,
        primary key (IDCONGE)
    );

    create table COURS (
        IDCOURS varchar(255) not null,
        DATEADEFINIR bit default 0 not null,
        DATECREATIONCOURS varbinary(255),
        DATEMODIFCOURS varbinary(255),
        DEBUTCOURS varbinary(255),
        DEMANDECOURSEXTERNE int,
        DUREEPREVUEENHEURES int,
        DUREEREELLEENHEURES int,
        FACTURATIONCOURS int,
        FINCOURS varbinary(255),
        LIBELLECOURS varchar(50),
        NBSTAGIAIRE int,
        IDECF int,
        IDFORMATEUR int,
        IDMODULE int not null,
        CODEPROMOTION varchar(255),
        CODESALLE varchar(5),
        primary key (IDCOURS)
    );

    create table ECF (
        IDECF int identity not null,
        ISCORRIGE bit default 0 not null,
        DATEECF varbinary(255),
        DATECREATION datetime,
        DATEMODIFICATIONECF datetime,
        LIBELLEECF varchar(200),
        IDCOURS varchar(255) not null,
        primary key (IDECF)
    );

    create table EQUIPEMENT (
        IDEQUIPEMENT int identity not null,
        CPUEQUIPEMENT varchar(15),
        DERNIERNETTOYAGE datetime,
        DISQUE varchar(50),
        FINGARANTIE datetime,
        LOTEQUIPEMENT varchar(4),
        MODELEEQUIPEMENT varchar(50),
        RAMEQUIPEMENT varchar(3),
        TYPEEQUIPEMENT varchar(30),
        IDSALLE varchar(5) not null,
        primary key (IDEQUIPEMENT)
    );

    create table EXPERIENCE (
        ISDEBUTANT bit default 0 not null,
        NBDISPENSE int not null,
        IDFORMATEUR int not null,
        IDMODULE int not null,
        primary key (IDFORMATEUR, IDMODULE)
    );

    create table FICHE_TEMPS (
        IDFICHETEMPS int identity not null,
        ANNEEFICHETEMPS int not null,
        ISARCHIVE bit default 0 not null,
        MOISFICHETEMPS int not null,
        ISVALIDE bit default 0 not null,
        FORMATEURID int not null,
        primary key (IDFICHETEMPS)
    );

    create table FORMATEUR (
        IDFORMATEUR int not null,
        ISEXTERNE bit default 0 not null,
        MAIL varchar(200),
        NOM varchar(100),
        PRENOM varchar(100),
        USERNAME varchar(50),
        IDHABILITATION int,
        primary key (IDFORMATEUR)
    );

    create table FORMATEUR_COMPETENCE (
        IDFORMATEUR int not null,
        CODECOMPETENCE varchar(20) not null
    );

    create table FORMATION (
        CODEFORMATION varchar(8) not null,
        ARCHIVERFORMATION bit default 0 not null,
        CODETITRE varchar(8),
        DATECREATIONFORMATION varbinary(255),
        DATEMODIFFORMATION varbinary(255),
        DUREEENHEURESFORMATION int,
        DUREEENSEMAINESFORMATION int,
        ECFAPASSER int,
        HEURESCENTRE int,
        HEURESSTAGE int,
        LIBELLECOURSFORMATION varchar(50),
        LIBELLELONG varchar(200),
        SEMAINESCENTRE int,
        SEMAINESTAGE int,
        TAUXHORAIRE float,
        TYPE varchar(255),
        primary key (CODEFORMATION)
    );

    create table HABILITATION (
        IDHABILITATION int identity not null,
        LIBELLEHABILITATION varchar(50),
        primary key (IDHABILITATION)
    );

    create table LIEU (
        IDLIEU int not null,
        LIBELLELIEU varchar(50),
        primary key (IDLIEU)
    );

    create table MODULE (
        IDMODULE int not null,
        ARCHIVER bit default 0 not null,
        DATECREATIONMODULE varbinary(255),
        DATEMODIFMODULE varbinary(255),
        DETAIL varchar(255),
        DUREEENHEURES int,
        DUREEENSEMAINES int,
        LIBELLEMODULE varchar(200),
        LIBELLECOURT varchar(20),
        primary key (IDMODULE)
    );

    create table MODULE_COMPETENCE (
        IDMODULE int not null,
        CODECOMPETENCE varchar(20) not null
    );

    create table MODULE_EQUIPEMENT (
        IDMODULE int not null,
        IDEQUIPEMENT int not null
    );

    create table NOTIFICATION (
        IDNOTIFICATION int identity not null,
        CODEALERTE int,
        message varchar(255),
        IDFORMATEUR int,
        MODULE int,
        IDPARAMETRE varchar(30),
        primary key (IDNOTIFICATION)
    );

    create table PARAMETRAGE (
        CODEPARAM varchar(30) not null,
        LIBELLEPARAM varchar(200) not null,
        TEXTEPARAM varchar(500) not null,
        TYPEPARAM varchar(15) not null,
        primary key (CODEPARAM)
    );

    create table PISTE_AUDIT (
        IDAUDIT int identity not null,
        ACTIONAUDIT varchar(1500),
        DATEAUDIT varbinary(255),
        USERAUDIT varchar(50),
        primary key (IDAUDIT)
    );

    create table PLANNING_BROUILLON (
        IDPLANNING int identity not null,
        DATECREATIONPLANNING varbinary(255),
        DEBUTCOURSBROUILLON varbinary(255),
        FINCOURSBROUILLON varbinary(255),
        LIBELLEPLANNING varchar(200),
        IDCOURS varchar(255),
        IDFORMATEUR int,
        CODESALLE varchar(5),
        primary key (IDPLANNING)
    );

    create table PROMOTION (
        CODEPROMOTION varchar(255) not null,
        DATECREATIONPROMOTION varbinary(255),
        DATEMODIFPROMOTION varbinary(255),
        DEBUTPROMOTION varbinary(255),
        FINPROMOTION varbinary(255),
        LIBELLEPROMOTION varchar(50),
        CODEFORMATION varchar(8),
        IDLIEU int,
        primary key (CODEPROMOTION)
    );

    create table SALLE (
        CODESALLE varchar(5) not null,
        CAPACITESALLE int,
        DISPONIBILITESALLE bit default 1 not null,
        LIBELLESALLE varchar(50),
        IDLIEU int not null,
        primary key (CODESALLE)
    );

    create table TYPE_ACTIVITE (
        CODEACTIVITE varchar(8) not null,
        LIBELLEACTIVITE varchar(200),
        primary key (CODEACTIVITE)
    );

    alter table ACTIVITE 
        add constraint fichetemps_date_periode_uniq unique (FICHETEMPSID, DATEACTIVITE, PERIODE);

    alter table AFFECTATION_LIEU 
        add constraint preferencesite_formateur_uniq unique (PREFERENCELIEU, IDFORMATEUR);

    alter table ECF 
        add constraint UK_4u34vda33lan5pjsy9feu6pu7 unique (IDCOURS);

    alter table FICHE_TEMPS 
        add constraint formateur_date_uniq unique (ANNEEFICHETEMPS, MOISFICHETEMPS, FORMATEURID);

    alter table FORMATEUR_COMPETENCE 
        add constraint form_comp_uniq unique (IDFORMATEUR, CODECOMPETENCE);

    alter table MODULE_COMPETENCE 
        add constraint module_comp_uniq unique (IDMODULE, CODECOMPETENCE);

    alter table MODULE_EQUIPEMENT 
        add constraint module_equip_uniq unique (IDMODULE, IDEQUIPEMENT);

    alter table ACTIVITE 
        add constraint FKc5olmwsiyxpqdcjkbn98ue23y 
        foreign key (FICHETEMPSID) 
        references FICHE_TEMPS;

    alter table ACTIVITE 
        add constraint FKm31qgdwq6g0ys8ipg7wdxf9cq 
        foreign key (CODEACTIVITE) 
        references TYPE_ACTIVITE;

    alter table AFFECTATION_LIEU 
        add constraint FK6m6a78tjqsb5yl2um41nh7ye9 
        foreign key (IDFORMATEUR) 
        references FORMATEUR;

    alter table AFFECTATION_LIEU 
        add constraint FKnjef2c5ei1p7yt5hasgwy3h4y 
        foreign key (IDLIEU) 
        references LIEU;

    alter table CONFIGURATION 
        add constraint FKtebe17xhcxv8nyv017ejvqv33 
        foreign key (CODEPARAM) 
        references PARAMETRAGE;

    alter table CONGE 
        add constraint FKg6mvmqir4e2tbat1qqnchqt5r 
        foreign key (FORMATEURID) 
        references FORMATEUR;

    alter table COURS 
        add constraint FK97ivacivowh15he8khr6mj4gm 
        foreign key (IDECF) 
        references ECF;

    alter table COURS 
        add constraint FKaet5uj3wqohbkfgtjxd0u9guf 
        foreign key (IDFORMATEUR) 
        references FORMATEUR;

    alter table COURS 
        add constraint FKnernmk9r2fkqhtyh9vbs20c8m 
        foreign key (IDMODULE) 
        references MODULE;

    alter table COURS 
        add constraint FKn6x8ejvyccmi1bdef9lvkgr33 
        foreign key (CODEPROMOTION) 
        references PROMOTION;

    alter table COURS 
        add constraint FKie3u6wbchcj0p4por7hh0xyim 
        foreign key (CODESALLE) 
        references SALLE;

    alter table ECF 
        add constraint FKf4yh7imya50a24c2o59h82lv9 
        foreign key (IDCOURS) 
        references COURS;

    alter table EQUIPEMENT 
        add constraint FKoipnn4vmh34iwcpw96plddbdq 
        foreign key (IDSALLE) 
        references SALLE;

    alter table EXPERIENCE 
        add constraint FKdhv08pkx20fm3nj0ugvrsnxre 
        foreign key (IDFORMATEUR) 
        references FORMATEUR;

    alter table EXPERIENCE 
        add constraint FKglum2ln4dlb6o0xvbj07h44dq 
        foreign key (IDMODULE) 
        references MODULE;

    alter table FICHE_TEMPS 
        add constraint FK4eaciucospgmemmhjp1fjefa4 
        foreign key (FORMATEURID) 
        references FORMATEUR;

    alter table FORMATEUR 
        add constraint FK7ht00l1tvrsor2jmp30qb56sp 
        foreign key (IDHABILITATION) 
        references HABILITATION;

    alter table FORMATEUR_COMPETENCE 
        add constraint FK4asixt2iepf26u656y4gqh1wl 
        foreign key (CODECOMPETENCE) 
        references COMPETENCE;

    alter table FORMATEUR_COMPETENCE 
        add constraint FKt6v540abw977hmst5q9p2h8lx 
        foreign key (IDFORMATEUR) 
        references FORMATEUR;

    alter table MODULE_COMPETENCE 
        add constraint FKjk9p3dw3aryglk2r1axf5jjee 
        foreign key (CODECOMPETENCE) 
        references COMPETENCE;

    alter table MODULE_COMPETENCE 
        add constraint FK7y5ao66k33gjeojnsd4rqgbgc 
        foreign key (IDMODULE) 
        references MODULE;

    alter table MODULE_EQUIPEMENT 
        add constraint FKk0gk5hxphe0r7qda1mpwnrakd 
        foreign key (IDEQUIPEMENT) 
        references EQUIPEMENT;

    alter table MODULE_EQUIPEMENT 
        add constraint FK42nrcrfa4sc0tn8cigamg8pr4 
        foreign key (IDMODULE) 
        references MODULE;

    alter table NOTIFICATION 
        add constraint FKpna09s0vrvawfpn2k3yjywj3v 
        foreign key (IDFORMATEUR) 
        references FORMATEUR;

    alter table NOTIFICATION 
        add constraint FKjo75janbbtqtgayjm0304ti0k 
        foreign key (MODULE) 
        references MODULE;

    alter table NOTIFICATION 
        add constraint FKthnf451ws83jojehm3ssu2iad 
        foreign key (IDPARAMETRE) 
        references PARAMETRAGE;

    alter table PLANNING_BROUILLON 
        add constraint FK11phdo3mpw4pexth6w67t10n8 
        foreign key (IDCOURS) 
        references COURS;

    alter table PLANNING_BROUILLON 
        add constraint FKtfrcokoakwswixof5kvph84m2 
        foreign key (IDFORMATEUR) 
        references FORMATEUR;

    alter table PLANNING_BROUILLON 
        add constraint FKhkkujhhfq1tgspnown9rsu1x8 
        foreign key (CODESALLE) 
        references SALLE;

    alter table PROMOTION 
        add constraint FKl6pp4owo3rwwb69916wmwp0dg 
        foreign key (CODEFORMATION) 
        references FORMATION;

    alter table PROMOTION 
        add constraint FKssanfdjqrmvpxtehv7x82wd1o 
        foreign key (IDLIEU) 
        references LIEU;

    alter table SALLE 
        add constraint FK15suk5g3lblfprwmtrbskoqht 
        foreign key (IDLIEU) 
        references LIEU;
