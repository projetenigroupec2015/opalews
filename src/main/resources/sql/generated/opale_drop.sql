
    alter table ACTIVITE 
        drop constraint FKc5olmwsiyxpqdcjkbn98ue23y;

    alter table ACTIVITE 
        drop constraint FKm31qgdwq6g0ys8ipg7wdxf9cq;

    alter table AFFECTATION_LIEU 
        drop constraint FK6m6a78tjqsb5yl2um41nh7ye9;

    alter table AFFECTATION_LIEU 
        drop constraint FKnjef2c5ei1p7yt5hasgwy3h4y;

    alter table CONFIGURATION 
        drop constraint FKtebe17xhcxv8nyv017ejvqv33;

    alter table CONGE 
        drop constraint FKg6mvmqir4e2tbat1qqnchqt5r;

    alter table COURS 
        drop constraint FK97ivacivowh15he8khr6mj4gm;

    alter table COURS 
        drop constraint FKaet5uj3wqohbkfgtjxd0u9guf;

    alter table COURS 
        drop constraint FKnernmk9r2fkqhtyh9vbs20c8m;

    alter table COURS 
        drop constraint FKn6x8ejvyccmi1bdef9lvkgr33;

    alter table COURS 
        drop constraint FKie3u6wbchcj0p4por7hh0xyim;

    alter table ECF 
        drop constraint FKf4yh7imya50a24c2o59h82lv9;

    alter table EQUIPEMENT 
        drop constraint FKoipnn4vmh34iwcpw96plddbdq;

    alter table EXPERIENCE 
        drop constraint FKdhv08pkx20fm3nj0ugvrsnxre;

    alter table EXPERIENCE 
        drop constraint FKglum2ln4dlb6o0xvbj07h44dq;

    alter table FICHE_TEMPS 
        drop constraint FK4eaciucospgmemmhjp1fjefa4;

    alter table FORMATEUR 
        drop constraint FK7ht00l1tvrsor2jmp30qb56sp;

    alter table FORMATEUR_COMPETENCE 
        drop constraint FK4asixt2iepf26u656y4gqh1wl;

    alter table FORMATEUR_COMPETENCE 
        drop constraint FKt6v540abw977hmst5q9p2h8lx;

    alter table MODULE_COMPETENCE 
        drop constraint FKjk9p3dw3aryglk2r1axf5jjee;

    alter table MODULE_COMPETENCE 
        drop constraint FK7y5ao66k33gjeojnsd4rqgbgc;

    alter table MODULE_EQUIPEMENT 
        drop constraint FKk0gk5hxphe0r7qda1mpwnrakd;

    alter table MODULE_EQUIPEMENT 
        drop constraint FK42nrcrfa4sc0tn8cigamg8pr4;

    alter table NOTIFICATION 
        drop constraint FKpna09s0vrvawfpn2k3yjywj3v;

    alter table NOTIFICATION 
        drop constraint FKjo75janbbtqtgayjm0304ti0k;

    alter table NOTIFICATION 
        drop constraint FKthnf451ws83jojehm3ssu2iad;

    alter table PLANNING_BROUILLON 
        drop constraint FK11phdo3mpw4pexth6w67t10n8;

    alter table PLANNING_BROUILLON 
        drop constraint FKtfrcokoakwswixof5kvph84m2;

    alter table PLANNING_BROUILLON 
        drop constraint FKhkkujhhfq1tgspnown9rsu1x8;

    alter table PROMOTION 
        drop constraint FKl6pp4owo3rwwb69916wmwp0dg;

    alter table PROMOTION 
        drop constraint FKssanfdjqrmvpxtehv7x82wd1o;

    alter table SALLE 
        drop constraint FK15suk5g3lblfprwmtrbskoqht;

    drop table ACTIVITE;

    drop table AFFECTATION_LIEU;

    drop table COMPETENCE;

    drop table CONFIGURATION;

    drop table CONGE;

    drop table COURS;

    drop table ECF;

    drop table EQUIPEMENT;

    drop table EXPERIENCE;

    drop table FICHE_TEMPS;

    drop table FORMATEUR;

    drop table FORMATEUR_COMPETENCE;

    drop table FORMATION;

    drop table HABILITATION;

    drop table LIEU;

    drop table MODULE;

    drop table MODULE_COMPETENCE;

    drop table MODULE_EQUIPEMENT;

    drop table NOTIFICATION;

    drop table PARAMETRAGE;

    drop table PISTE_AUDIT;

    drop table PLANNING_BROUILLON;

    drop table PROMOTION;

    drop table SALLE;

    drop table TYPE_ACTIVITE;
