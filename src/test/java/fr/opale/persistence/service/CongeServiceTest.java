package fr.opale.persistence.service;

/**
 * Created by Brice BRUNEAU
 * Date : 15/07/2016
 * Package : fr.opale.persistence.service
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = MainLauncher.class)
//@WebIntegrationTest
//@IntegrationTest("server.port=0")
//@Transactional
//@Rollback()
public class CongeServiceTest {

//    @Autowired
//    CongeService service;
//
//    @Autowired
//    HabilitationDao habilitationDao;
//
//    @Autowired
//    FormateurService formateurService;
//
//
//    private Formateur formateurDefault;
//    private Formateur formateurSecondary;
//    private Conge congeTest;
//    private Collection<Conge> congesTest;
//
//    private Conge congeBefore;
//    private Conge congeAfter;
//    private Conge congeDuring;
//    private Conge congeIn;
//    private Conge congeOnStart;
//    private Conge congeOnEnd;
//    private Conge congeFormateur2;
//
//    @Before
//    public void initializeData() {
//        Habilitation habilitation = new Habilitation("habilitation test", 747);
//        habilitationDao.save(habilitation);
//
//        Formateur formateur = new Formateur();
//        formateur.setHabilitation(habilitation);
//        formateur.setUsername("pdupont");
//        formateur.setPrenom("Patrick");
//        formateur.setNom("DUPONT");
//        formateur.setMail("pdupont@eni.local");
//        formateur.setExterne(false);
//        this.formateurService.create(formateur);
//        this.formateurDefault = formateur;
//
//        Formateur formateur2 = new Formateur();
//        formateur2.setHabilitation(habilitation);
//        formateur2.setUsername("pdupont");
//        formateur2.setPrenom("Pascal");
//        formateur2.setNom("DURANT");
//        formateur2.setMail("pdurant@eni.local");
//        formateur2.setExterne(false);
//        this.formateurService.create(formateur2);
//        this.formateurSecondary = formateur2;
//
//        LocalDateTime dateDeb = LocalDateTime.now();
//        LocalDateTime dateFin = dateDeb.plusMonths(1).plusDays(6).plusHours(2).plusMinutes(23).plusSeconds(52);
//
//        this.congeTest = getConge(dateDeb, dateFin);
//        this.congeBefore = getConge(dateDeb.minusMonths(2), dateDeb.minusDays(2));
//        this.congeAfter = getConge(dateFin.plusDays(1), dateFin.plusDays(18));
//        this.congeDuring = getConge(dateDeb.minusDays(5), dateFin.plusDays(12).plusHours(3));
//        this.congeIn = getConge(dateDeb.plusDays(5), dateFin.minusDays(5).minusHours(3));
//        this.congeOnEnd = getConge(dateFin.minusDays(1), dateFin.plusMonths(1));
//        this.congeOnStart = getConge(dateDeb.minusDays(6), dateDeb.plusHours(2));
//
//        this.congeFormateur2 =  getConge(dateDeb.plusDays(5), dateFin.minusDays(5).minusHours(3));
//        this.congeFormateur2.setFormateur(formateurSecondary);
//
//        this.congesTest = new HashSet<Conge>();
//        this.congesTest.add(congeBefore);
//        this.congesTest.add(congeAfter);
//        this.congesTest.add(congeDuring);
//        this.congesTest.add(congeIn);
//        this.congesTest.add(congeOnEnd);
//        this.congesTest.add(congeOnStart);
//        this.congesTest.add(congeTest);
//        this.congesTest.add(congeFormateur2);
//    }
//
//    /**
//     * Test the request to get "Conges" in a period
//     */
//    @Rollback()
//    @Test
//    public void findInPeriod() {
//        createCongesTest();
//        Collection<Conge> conges = service.findInPeriod(congeTest.getDateDebutConge(), congeTest.getDateFinConge());
//        overlappingCongesOfCongeTest(conges, true);
//    }
//
//    /**
//     * Test the request to get "Conges" in a period for a specific former
//     */
//    @Rollback()
//    @Test
//    public void findByFormateurInPeriod() {
//        createCongesTest();
//        Collection<Conge> conges = service.findByFormateurInPeriod(
//                congeTest.getFormateur(),
//                congeTest.getDateDebutConge(),
//                congeTest.getDateFinConge()
//        );
//        overlappingCongesOfCongeTest(conges, true);
//        if (conges != null) {
//            for(Conge conge : conges) {
//                Assert.assertTrue(formateurDefault.equals(conge.getFormateur()));
//            }
//            Assert.assertFalse(conges.contains(congeFormateur2));
//            conges = service.findInPeriod(congeTest.getDateDebutConge(), congeTest.getDateFinConge());
//            Assert.assertTrue(conges.contains(congeFormateur2));
//        } else {
//            Assert.assertTrue(false);
//        }
//    }
//
//    /**
//     * Test the validity control method of a "Conge"
//     */
//    @Rollback()
//    @Test
//    public void isValidConge1() {
//        service.create(congeBefore);
//        service.create(congeAfter);
//        service.create(congeTest);
//        service.create(congeFormateur2);
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        deleteNotInCongesTest(conges);
//        Assert.assertTrue(service.isValidConge(congeTest));
//    }
//
//    /**
//     * Test the validity control method of a "Conge"
//     */
//    @Rollback()
//    @Test
//    public void isValidConge2() {
//        service.create(congeOnStart);
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        deleteNotInCongesTest(conges);
//        Assert.assertFalse(service.isValidConge(congeTest));
//    }
//
//    /**
//     * Test the validity control method of a "Conge"
//     */
//    @Rollback()
//    @Test
//    public void isValidConge3() {
//        service.create(congeOnEnd);
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        deleteNotInCongesTest(conges);
//        Assert.assertFalse(service.isValidConge(congeTest));
//    }
//
//    /**
//     * Test the validity control method of a "Conge"
//     */
//    @Rollback()
//    @Test
//    public void isValidConge4() {
//        service.create(congeIn);
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        deleteNotInCongesTest(conges);
//        Assert.assertFalse(service.isValidConge(congeTest));
//    }
//
//    /**
//     * Test the validity control method of a "Conge"
//     */
//    @Rollback()
//    @Test
//    public void isValidConge5() {
//        service.create(congeDuring);
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        deleteNotInCongesTest(conges);
//        Assert.assertFalse(service.isValidConge(congeTest));
//    }
//
//    /**
//     * Test the validity control method of a "Conge"
//     */
//    @Rollback()
//    @Test
//    public void isValidConge6() {
//        createCancelCongesTest();
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        deleteNotInCongesTest(conges);
//        Assert.assertTrue(service.isValidConge(congeTest));
//    }
//
//    /**
//     * Test the request to get not cancel "Conges" in a period for a specific former
//     */
//    @Rollback()
//    @Test
//    public void findActiveByFormateurInPeriod1() {
//        createValidCongesTest();
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        if (conges != null) {
//            for(Conge conge : conges) {
//                Assert.assertTrue(conge.getEtatConge() != EtatCongeEnum.REFUSE);
//                Assert.assertTrue(formateurDefault.equals(conge.getFormateur()));
//            }
//            overlappingCongesOfCongeTest(conges, true);
//        } else {
//            Assert.assertTrue(false);
//        }
//    }
//
//    /**
//     * Test the request to get not cancel "Conges" in a period for a specific former
//     */
//    @Rollback()
//    @Test
//    public void findActiveByFormateurInPeriod2() {
//        createCancelCongesTest();
//        Collection<Conge> conges = getFindActiveByFormateurInPeriodFromCongeTest();
//        if (conges != null) {
//            for(Conge conge : conges) {
//                Assert.assertTrue(conge.getEtatConge() != EtatCongeEnum.REFUSE);
//                Assert.assertTrue(formateurDefault.equals(conge.getFormateur()));
//            }
//            allCongesTestAbsent(conges);
//        }
//    }
//
//    /**
//     * Test the request to get not cancel "Conges" in a period
//     */
//    @Rollback()
//    @Test
//    public void findActiveInPeriod() {
//        createCancelCongesTest();
//        Collection<Conge> conges = service.findActiveInPeriod(
//                congeTest.getDateDebutConge(),
//                congeTest.getDateFinConge()
//        );
//        if (conges != null) {
//            for(Conge conge : conges) {
//                Assert.assertTrue(conge.getEtatConge() != EtatCongeEnum.REFUSE);
//            }
//            allCongesTestAbsent(conges);
//        }
//    }
//
//    /**
//     * Test the request to get "Conges" in a period with specific state
//     */
//    @Rollback()
//    @Test
//    public void findByEtatCongeInPeriod1() {
//        createValidCongesTest();
//        Collection<Conge> conges = service.findByEtatCongeInPeriod(
//                EtatCongeEnum.VALIDE,
//                congeTest.getDateDebutConge(),
//                congeTest.getDateFinConge()
//        );
//        if (conges != null) {
//            for(Conge conge : conges) {
//                Assert.assertTrue(conge.getEtatConge() == EtatCongeEnum.VALIDE);
//            }
//            overlappingCongesOfCongeTest(conges, true);
//        } else {
//            Assert.assertTrue(false);
//        }
//    }
//
//    /**
//     * Test the request to get "Conges" in a period with specific state
//     */
//    @Rollback()
//    @Test
//    public void findByEtatCongeInPeriod2() {
//        createValidCongesTest();
//        Collection<Conge> conges = service.findByEtatCongeInPeriod(
//                EtatCongeEnum.REFUSE,
//                congeTest.getDateDebutConge(),
//                congeTest.getDateFinConge()
//        );
//        if (conges != null) {
//            for(Conge conge : conges) {
//                Assert.assertTrue(conge.getEtatConge() == EtatCongeEnum.REFUSE);
//            }
//
//            Assert.assertFalse(conges.contains(congeOnStart));
//            Assert.assertFalse(conges.contains(congeOnEnd));
//            Assert.assertFalse(conges.contains(congeIn));
//            Assert.assertFalse(conges.contains(congeDuring));
//            Assert.assertFalse(conges.contains(congeBefore));
//            Assert.assertFalse(conges.contains(congeAfter));
//        }
//    }
//
//    /**
//     * Test the request to get "Conges" in a period with specific state and former
//     */
//    @Rollback()
//    @Test
//    public void findByFormateurAndEtatCongeInPeriod() {
//        createCancelCongesTest();
//        Collection<Conge> conges = service.findByFormateurAndEtatCongeInPeriod(
//                congeTest.getFormateur(),
//                EtatCongeEnum.VALIDE,
//                congeTest.getDateDebutConge(),
//                congeTest.getDateFinConge()
//        );
//        if (conges != null) {
//            for(Conge conge : conges) {
//                Assert.assertTrue(conge.getEtatConge() != EtatCongeEnum.REFUSE);
//            }
//            allCongesTestAbsent(conges);
//        }
//    }
//
//
//    private Collection<Conge> getFindActiveByFormateurInPeriodFromCongeTest() {
//        return service.findActiveByFormateurInPeriod(
//                congeTest.getFormateur(),
//                congeTest.getDateDebutConge(),
//                congeTest.getDateFinConge()
//        );
//    }
//
//    private void overlappingCongesOfCongeTest(Collection<Conge> conges, boolean expectedResult) {
//        if (conges == null) {
//            Assert.assertTrue(!expectedResult);
//        } else {
//            Assert.assertTrue(conges.contains(congeOnStart) == expectedResult);
//            Assert.assertTrue(conges.contains(congeOnEnd)  == expectedResult);
//            Assert.assertTrue(conges.contains(congeIn) == expectedResult);
//            Assert.assertTrue(conges.contains(congeDuring) == expectedResult);
//
//            Assert.assertFalse(conges.contains(congeBefore) == expectedResult);
//            Assert.assertFalse(conges.contains(congeAfter) == expectedResult);
//        }
//    }
//
//    private void deleteNotInCongesTest(Collection<Conge> conges) {
//        if (conges != null) {
//            for (Conge conge : conges) {
//                if (!congesTest.contains(conge)) {
//                    service.delete(conge);
//                }
//            }
//        }
//    }
//
//    private void allCongesTestAbsent(Collection<Conge> conges) {
//        if (conges != null) {
//            Assert.assertFalse(conges.contains(congeOnStart));
//            Assert.assertFalse(conges.contains(congeOnEnd));
//            Assert.assertFalse(conges.contains(congeIn));
//            Assert.assertFalse(conges.contains(congeDuring));
//            Assert.assertFalse(conges.contains(congeBefore));
//            Assert.assertFalse(conges.contains(congeAfter));
//        }
//    }
//
//    private void createCongesTest() {
//        service.create(congeBefore);
//        service.create(congeAfter);
//        service.create(congeOnStart);
//        service.create(congeOnEnd);
//        service.create(congeIn);
//        service.create(congeDuring);
//        service.create(congeFormateur2);
//    }
//
//    private void createValidCongesTest() {
//        congeBefore.setEtatConge(EtatCongeEnum.VALIDE);
//        congeAfter.setEtatConge(EtatCongeEnum.VALIDE);
//        congeOnStart.setEtatConge(EtatCongeEnum.VALIDE);
//        congeOnEnd.setEtatConge(EtatCongeEnum.VALIDE);
//        congeIn.setEtatConge(EtatCongeEnum.VALIDE);
//        congeDuring.setEtatConge(EtatCongeEnum.VALIDE);
//        congeFormateur2.setEtatConge(EtatCongeEnum.VALIDE);
//        createCongesTest();
//    }
//
//    private void createCancelCongesTest() {
//        congeBefore.setEtatConge(EtatCongeEnum.REFUSE);
//        congeAfter.setEtatConge(EtatCongeEnum.REFUSE);
//        congeOnStart.setEtatConge(EtatCongeEnum.REFUSE);
//        congeOnEnd.setEtatConge(EtatCongeEnum.REFUSE);
//        congeIn.setEtatConge(EtatCongeEnum.REFUSE);
//        congeDuring.setEtatConge(EtatCongeEnum.REFUSE);
//        congeFormateur2.setEtatConge(EtatCongeEnum.REFUSE);
//        createCongesTest();
//    }
//
//    private Conge getConge(LocalDateTime dateStart, LocalDateTime dateEnd) {
//        Conge conge = new Conge();
//        conge.setFormateur(formateurDefault);
//        conge.setDateDemandeConge(LocalDateTime.now());
//        conge.setEtatConge(EtatCongeEnum.VALIDE);
//        conge.setMotif("vacances");
//        conge.setDateDebutConge(dateStart);
//        conge.setDateFinConge(dateEnd);
//        return conge;
//    }
}
