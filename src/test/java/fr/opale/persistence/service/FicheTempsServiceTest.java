package fr.opale.persistence.service;

import fr.opale.MainLauncher;
import fr.opale.enumeration.PeriodeActiviteEnum;
import fr.opale.persistence.dao.HabilitationDao;
import fr.opale.persistence.model.*;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by Brice BRUNEAU
 * Date : 31/07/2016
 * Package : fr.opale.persistence.service
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = MainLauncher.class)
//@WebIntegrationTest
//@IntegrationTest("server.port=0")
//@Transactional
//@Rollback()
public class FicheTempsServiceTest {
//
//    @Autowired
//    FicheTempsService service;
//
//    @Autowired
//    HabilitationDao habilitationDao;
//
//    @Autowired
//    FormateurService formateurService;
//
//    @Autowired
//    ActiviteTypeService activiteTypeService;
//
//
//    private Formateur formateurDefault;
//    private Formateur formateurSecondary;
//
//    private LocalDate date  = LocalDate.now();
//
//    private TypeActivite typeActiviteDefault;
//    private TypeActivite typeActiviteSecondary;
//
//    private Activite activite1;
//    private Activite activite2;
//    private Activite activite3;
//    private Activite activite4;
//    private Activite activite5;
//    private Activite activite6;
//    private Activite activite7;
//
//    private FicheTemps ficheTemps1F1;
//    private FicheTemps ficheTemps2F1;
//    private FicheTemps ficheTemps3F1;
//    private FicheTemps ficheTemps4F1;
//    private FicheTemps ficheTemps1F2;
//    private FicheTemps ficheTemps2F2;
//
//
//    @Before
//    public void initializeData() {
//        Habilitation habilitation = new Habilitation("habilitation test", 747);
//        habilitationDao.save(habilitation);
//
//        Formateur formateur = new Formateur();
//        formateur.setHabilitation(habilitation);
//        formateur.setUsername("pdupont");
//        formateur.setPrenom("Patrick");
//        formateur.setNom("DUPONT");
//        formateur.setMail("pdupont@eni.local");
//        formateur.setExterne(false);
//        this.formateurService.create(formateur);
//        this.formateurDefault = formateur;
//
//        Formateur formateur2 = new Formateur();
//        formateur2.setHabilitation(habilitation);
//        formateur2.setUsername("pdurant");
//        formateur2.setPrenom("Pascal");
//        formateur2.setNom("DURANT");
//        formateur2.setMail("pdurant@eni.local");
//        formateur2.setExterne(false);
//        this.formateurService.create(formateur2);
//        this.formateurSecondary = formateur2;
//
//        this.typeActiviteDefault = new TypeActivite();
//        this.typeActiviteDefault.setCodeActivite("CO_" + formateur.getIdFormateur());
//        this.typeActiviteDefault.setLibelleActivite("Type Test1");
//        this.activiteTypeService.create(this.typeActiviteDefault);
//
//        this.typeActiviteSecondary = new TypeActivite();
//        this.typeActiviteSecondary.setCodeActivite("CO_" + formateur2.getIdFormateur());
//        this.typeActiviteSecondary.setLibelleActivite("Type Test2");
//        this.activiteTypeService.create(this.typeActiviteSecondary);
//
//        this.activite1 = getActivite(PeriodeActiviteEnum.MATIN, date.minusDays(1));
//        this.activite2 = getActivite(PeriodeActiviteEnum.APRES_MIDI, date.minusDays(1));
//        this.activite3 = getActivite(PeriodeActiviteEnum.MATIN, date.minusDays(2));
//        this.activite4 = getActivite(PeriodeActiviteEnum.MATIN, date.minusDays(3));
//        this.activite5 = getActivite(PeriodeActiviteEnum.APRES_MIDI, date.minusDays(3));
//        this.activite6 = getActivite(PeriodeActiviteEnum.MATIN, date.minusDays(4));
//        this.activite7 = getActivite(PeriodeActiviteEnum.APRES_MIDI, date.minusDays(4));
//
//        Collection<Activite> activites1 = new HashSet<>();
//        activites1.add(this.activite1);
//        activites1.add(this.activite2);
//
//        Collection<Activite> activites2 = new HashSet<>();
//        activites2.add(this.activite3);
//        activites2.add(this.activite4);
//        activites2.add(this.activite5);
//
//        Collection<Activite> activites3 = new HashSet<>();
//        activites3.add(this.activite6);
//        activites3.add(this.activite7);
//
//        this.ficheTemps1F1 = getFicheTemps(this.formateurDefault, this.date.getMonth().getValue(), this.date.getYear(), activites1, false, false);
//        this.ficheTemps2F1 = getFicheTemps(this.formateurDefault, this.date.minusMonths(1).getMonth().getValue(), this.date.getYear(), null, false, true);
//        this.ficheTemps3F1 = getFicheTemps(this.formateurDefault, this.date.minusMonths(2).getMonth().getValue(), this.date.getYear(), activites2, false, true);
//        this.ficheTemps4F1 = getFicheTemps(this.formateurDefault, this.date.minusMonths(3).getMonth().getValue(), this.date.getYear(), null, true, true);
//        this.ficheTemps1F2 = getFicheTemps(this.formateurSecondary, this.date.getMonth().getValue(), this.date.getYear(), null, false, true);
//        this.ficheTemps2F2 = getFicheTemps(this.formateurSecondary, this.date.minusMonths(1).getMonth().getValue(), this.date.getYear(), activites3, true, false);
//    }
//
//
//    @Rollback()
//    @Test
//    public void findByArchivedAndDate() {
//        this.ficheTemps1F1.setArchive(true);
//        this.ficheTemps1F2.setArchive(false);
//        this.ficheTemps2F1.setArchive(true);
//        this.ficheTemps3F1.setArchive(false);
//        this.ficheTemps4F1.setArchive(false);
//        this.ficheTemps2F2.setArchive(true);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByArchivedAndDate(true, this.date.getMonth().getValue(), this.date.getYear());
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            Assert.assertTrue(ficheTemps.getMoisFicheTemps() == this.date.getMonth().getValue());
//            Assert.assertTrue(ficheTemps.getAnneeFicheTemps() == this.date.getYear());
//            Assert.assertTrue(ficheTemps.isArchive());
//        }
//
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByArchivedAndValidAndDate() {
//        this.ficheTemps1F1.setArchive(false);
//        this.ficheTemps1F1.setValide(true);
//        this.ficheTemps1F2.setArchive(false);
//        this.ficheTemps1F2.setValide(true);
//        this.ficheTemps2F1.setArchive(false);
//        this.ficheTemps2F1.setValide(true);
//        this.ficheTemps2F2.setArchive(false);
//        this.ficheTemps2F2.setValide(false);
//        this.ficheTemps3F1.setArchive(false);
//        this.ficheTemps3F1.setValide(true);
//        this.ficheTemps4F1.setArchive(true);
//        this.ficheTemps4F1.setValide(true);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByArchivedAndValidAndDate(
//                false,
//                true,
//                this.date.getMonthValue(),
//                this.date.getYear()
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            Assert.assertTrue(ficheTemps.getMoisFicheTemps() == this.date.getMonth().getValue());
//            Assert.assertTrue(ficheTemps.getAnneeFicheTemps() == this.date.getYear());
//            Assert.assertFalse(ficheTemps.isArchive());
//            Assert.assertTrue(ficheTemps.isValide());
//        }
//
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findInPeriod() {
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findInPeriod(
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByArchivedInPeriod() {
//        this.ficheTemps1F1.setArchive(true);
//        this.ficheTemps1F2.setArchive(false);
//        this.ficheTemps2F1.setArchive(true);
//        this.ficheTemps3F1.setArchive(false);
//        this.ficheTemps4F1.setArchive(false);
//        this.ficheTemps2F2.setArchive(true);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByArchivedInPeriod(
//                false,
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//            Assert.assertTrue(!ficheTemps.isArchive());
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByFormateurInPeriod() {
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByFormateurInPeriod(
//                formateurDefault,
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//            Assert.assertTrue(ficheTemps.getFormateur().equals(this.formateurDefault));
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByFormateurAndArchivedInPeriod() {
//        this.ficheTemps1F1.setArchive(true);
//        this.ficheTemps1F2.setArchive(false);
//        this.ficheTemps2F1.setArchive(true);
//        this.ficheTemps3F1.setArchive(false);
//        this.ficheTemps4F1.setArchive(false);
//        this.ficheTemps2F2.setArchive(true);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByFormateurAndArchivedInPeriod(
//                formateurDefault,
//                false,
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//            Assert.assertTrue(!ficheTemps.isArchive());
//            Assert.assertTrue(ficheTemps.getFormateur().equals(this.formateurDefault));
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByDate() {
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByDate(this.date.getMonth().getValue(), this.date.getYear());
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            Assert.assertTrue(ficheTemps.getMoisFicheTemps() == this.date.getMonth().getValue());
//            Assert.assertTrue(ficheTemps.getAnneeFicheTemps() == this.date.getYear());
//        }
//
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByFormateurAndDate() {
//        createFichesTemps();
//
//        FicheTemps ficheTemps = service.findByFormateurAndDate(
//                this.formateurSecondary,
//                this.date.getMonth().getValue(),
//                this.date.getYear()
//        );
//
//        if (ficheTemps != null) {
//            Assert.assertTrue(ficheTemps.getMoisFicheTemps() == this.date.getMonth().getValue());
//            Assert.assertTrue(ficheTemps.getAnneeFicheTemps() == this.date.getYear());
//            Assert.assertTrue(ficheTemps.getFormateur().equals(this.formateurSecondary));
//            Assert.assertTrue(ficheTemps.equals(this.ficheTemps1F2));
//        } else {
//            Assert.assertTrue(false);
//        }
//    }
//
//    @Rollback()
//    @Test
//    public void findByValidInPeriod() {
//        this.ficheTemps1F1.setValide(true);
//        this.ficheTemps2F1.setValide(true);
//        this.ficheTemps3F1.setValide(false);
//        this.ficheTemps4F1.setValide(false);
//        this.ficheTemps1F2.setValide(false);
//        this.ficheTemps2F2.setValide(true);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByValidInPeriod(
//                true,
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//            Assert.assertTrue(ficheTemps.isValide());
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByArchivedAndValidInPeriod() {
//        this.ficheTemps1F1.setValide(true);
//        this.ficheTemps2F1.setValide(true);
//        this.ficheTemps3F1.setValide(false);
//        this.ficheTemps4F1.setValide(false);
//        this.ficheTemps1F2.setValide(false);
//        this.ficheTemps2F2.setValide(true);
//        this.ficheTemps1F1.setArchive(true);
//        this.ficheTemps1F2.setArchive(false);
//        this.ficheTemps2F1.setArchive(true);
//        this.ficheTemps3F1.setArchive(false);
//        this.ficheTemps4F1.setArchive(true);
//        this.ficheTemps2F2.setArchive(false);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByArchivedAndValidInPeriod(
//                true,
//                true,
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//            Assert.assertTrue(ficheTemps.isArchive());
//            Assert.assertTrue(ficheTemps.isValide());
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByFormateurAndValidInPeriod() {
//        this.ficheTemps1F1.setValide(true);
//        this.ficheTemps2F1.setValide(true);
//        this.ficheTemps3F1.setValide(false);
//        this.ficheTemps4F1.setValide(false);
//        this.ficheTemps1F2.setValide(false);
//        this.ficheTemps2F2.setValide(true);
//
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByFormateurAndValidInPeriod(
//                this.formateurDefault,
//                true,
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//            Assert.assertTrue(ficheTemps.isValide());
//            Assert.assertTrue(ficheTemps.getFormateur().equals(this.formateurDefault));
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByFormateurAndArchivedAndValidInPeriod() {
//        this.ficheTemps1F1.setValide(true);
//        this.ficheTemps2F1.setValide(true);
//        this.ficheTemps3F1.setValide(true);
//        this.ficheTemps4F1.setValide(false);
//        this.ficheTemps1F2.setValide(false);
//        this.ficheTemps2F2.setValide(true);
//        this.ficheTemps1F1.setArchive(true);
//        this.ficheTemps1F2.setArchive(false);
//        this.ficheTemps2F1.setArchive(true);
//        this.ficheTemps3F1.setArchive(false);
//        this.ficheTemps4F1.setArchive(true);
//        this.ficheTemps2F2.setArchive(true);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByFormateurAndArchivedAndValidInPeriod(
//                this.formateurDefault,
//                true,
//                true,
//                this.date.minusMonths(2).getMonthValue(),
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                this.date.getYear()
//        );
//
//        LocalDate dateStart = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(2).getMonthValue(),
//                1
//        );
//        LocalDate dateEnd = LocalDate.of(
//                this.date.getYear(),
//                this.date.minusMonths(1).getMonthValue(),
//                1
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            LocalDate dateFiche = LocalDate.of(
//                    ficheTemps.getAnneeFicheTemps(),
//                    ficheTemps.getMoisFicheTemps(),
//                    1
//            );
//            Assert.assertTrue(dateFiche.compareTo(dateStart) > -1);
//            Assert.assertTrue(dateFiche.compareTo(dateEnd) < 1);
//            Assert.assertTrue(ficheTemps.isArchive());
//            Assert.assertTrue(ficheTemps.isValide());
//            Assert.assertTrue(ficheTemps.getFormateur().equals(this.formateurDefault));
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    @Rollback()
//    @Test
//    public void findByValidAndDate() {
//        this.ficheTemps1F1.setValide(true);
//        this.ficheTemps1F2.setValide(false);
//        this.ficheTemps2F1.setValide(false);
//        this.ficheTemps3F1.setValide(true);
//        this.ficheTemps4F1.setValide(false);
//        this.ficheTemps2F2.setValide(true);
//        createFichesTemps();
//        Collection<FicheTemps> fichesTemps = service.findByValidAndDate(
//                true,
//                this.date.minusMonths(1).getMonth().getValue(),
//                this.date.getYear()
//        );
//
//        for (FicheTemps ficheTemps : fichesTemps) {
//            Assert.assertTrue(ficheTemps.getMoisFicheTemps() == this.date.minusMonths(1).getMonth().getValue());
//            Assert.assertTrue(ficheTemps.getAnneeFicheTemps() == this.date.getYear());
//            Assert.assertTrue(ficheTemps.isArchive());
//        }
//
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps2F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps3F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps4F1));
//        Assert.assertFalse(fichesTemps.contains(this.ficheTemps1F2));
//        Assert.assertTrue(fichesTemps.contains(this.ficheTemps2F2));
//    }
//
//    private void createFichesTemps() {
//        service.create(this.ficheTemps1F1);
//        service.create(this.ficheTemps2F1);
//        service.create(this.ficheTemps3F1);
//        service.create(this.ficheTemps4F1);
//        service.create(this.ficheTemps1F2);
//        service.create(this.ficheTemps2F2);
//    }
//
//    private Activite getActivite(PeriodeActiviteEnum periode, LocalDate date) {
//        Activite activite = new Activite();
//        activite.setTypeActivite(this.typeActiviteDefault);
//        activite.setPeriodeActivite(periode);
//        activite.setDateActivite(date);
//        return activite;
//    }
//
//    private FicheTemps getFicheTemps(Formateur formateur, int month, int year, Collection<Activite> activites, boolean archived, boolean valid) {
//        FicheTemps ficheTemps = new FicheTemps();
//        ficheTemps.setFormateur(formateur);
//        ficheTemps.setArchive(archived);
//        ficheTemps.setValide(valid);
//        ficheTemps.setMoisFicheTemps(month);
//        ficheTemps.setAnneeFicheTemps(year);
//        ficheTemps.setActivites(activites);
//        return ficheTemps;
//    }
}
