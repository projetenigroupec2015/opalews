package fr.opale.persistence.service;

/**
 * Created by Thomas Croguennec
 * Date : 26/06/16
 * Package : fr.opale.persistence.service
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = MainLauncher.class)
//@WebIntegrationTest
//@IntegrationTest("server.port=0")
//@Transactional
//@Rollback(true)
public class ParametrageServiceTest {

//    @Autowired
//    private ParametrageService service;
//
//    Parametrage parametrage1;
//    Parametrage parametrage2;
//    Parametrage parametrage3;
//    Configuration configuration1;
//    Configuration configuration2;
//    Configuration configuration3;
//
//
//    @Before
//    public void setUp() throws Exception {
//        parametrage1 = new Parametrage();
//        parametrage1.setCode("codeParam");
//        parametrage1.setLibelle("libelleParam");
//        parametrage1.setTexteParam("texteParam");
//        parametrage1.setType("alerte");
//        configuration1 = new Configuration();
//        configuration1.setValeur("valeur Test");
//        configuration1.setUnite("unité test");
//        configuration1.setParametre(parametrage1);
//        List<Configuration> configs = new ArrayList<Configuration>();
//        configs.add(configuration1);
//        parametrage1.setConfigs(configs);
//
//        service.create(parametrage1);
//
//        parametrage3 = new Parametrage();
//        parametrage3.setCode("codeParam3");
//        parametrage3.setLibelle("libelleParam");
//        parametrage3.setTexteParam("texteParam");
//        parametrage3.setType("alerte");
//
//        configuration3 = new Configuration();
//        configuration3.setValeur("valeur Test");
//        configuration3.setUnite("unité test");
//        configuration3.setParametre(parametrage3);
//        List<Configuration> configs3 = new ArrayList<Configuration>();
//        configs.add(configuration3);
//
//        parametrage3.setConfigs(configs3);
//
//        service.create(parametrage3);
//    }
//
//    @Test
//    public void findAllAlertes() throws Exception {
//        List<Parametrage> paramTests = service.findAllAlertes();
//        Assert.assertNotNull(paramTests);
//        Assert.assertThat(paramTests.size(), IsEqual.equalTo(2));
//
//    }
//
//    @Test
//    public void updateConfiguration() throws Exception {
//        Configuration modifConfig = configuration1;
//        modifConfig.setValeur("valeur modif");
//        int modif = service.updateConfiguration(modifConfig.getParametre().getCode(), modifConfig.getValeur());
//        Assert.assertThat("valeur modif", IsEqual.equalTo("valeur modif"));
//    }
//
//    @Test
//    public void findOne() throws Exception {
//        Parametrage paramResult = service.findOne(parametrage1.getCode());
//        Assert.assertNotNull(paramResult);
//        Assert.assertThat(parametrage1.getCode(), IsEqual.equalTo(paramResult.getCode()));
//    }
//
//    @Test
//    public void findAll() throws Exception {
//        List<Parametrage> params = service.findAll();
//        Assert.assertNotNull(params);
//        Assert.assertThat(params.size(), IsEqual.equalTo(2));
//    }
//
//    @Test
//    public void create() throws Exception {
//        parametrage2 = new Parametrage();
//        parametrage2.setCode("codeParam2");
//        parametrage2.setLibelle("libelleParam");
//        parametrage2.setTexteParam("texteParam");
//        parametrage2.setType("TypeParam");
//
//        configuration2 = new Configuration();
//        configuration2.setValeur("valeur Test2");
//        configuration2.setUnite("unité test2");
//        configuration2.setParametre(parametrage2);
//        List<Configuration> configs = new ArrayList<Configuration>();
//        configs.add(configuration2);
//
//        parametrage2.setConfigs(configs);
//
//        Parametrage paramResult = service.create(parametrage2);
//        Assert.assertThat(paramResult.getCode(), IsEqual.equalTo(parametrage2.getCode()));
//    }
//
//    @Test
//    public void getTotal() throws Exception {
//        int total = service.getTotal();
//        Assert.assertThat(total, IsEqual.equalTo(2));
//    }
//
//    @Test
//    public void isExisting() throws Exception {
//        boolean exist = service.isExisting(parametrage1.getCode());
//        Assert.assertThat(exist, IsEqual.equalTo(true));
//    }
//
//    @Test
//    public void delete() throws Exception {
//        int total = service.getTotal();
//        service.delete(parametrage3);
//        int totalAfterDelete = service.getTotal();
//        Assert.assertThat(totalAfterDelete, IsNot.not(total));
//    }
//
//    @Test
//    public void deleteById() throws Exception {
//        int total = service.getTotal();
//        service.deleteById(parametrage1.getCode());
//        int totalAfterDelete = service.getTotal();
//        Assert.assertThat(totalAfterDelete, IsNot.not(total));
//    }
}