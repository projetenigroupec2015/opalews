package fr.opale;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MainLauncher.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class OpaleWsApplicationTests {

	@Autowired
	WebApplicationContext wac;

	@Test
	public void contextLoads() {
		Assert.assertNotNull(wac);
	}

}
